// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_REACT_UTILITY_MULTIFUNCTION_HH
#define DUNE_REACT_UTILITY_MULTIFUNCTION_HH

#include <cstddef>
#include <memory>
#include <tuple>
#include <typeinfo>
#include <utility>

namespace Dune {
  namespace React {

    namespace MultiFunctionImpl {

      // Handler virtual base class
      template<class... Signatures>
      struct HandlerBaseImpl;

      template<class R0, class... Args0>
      struct HandlerBaseImpl<R0(Args0...)>
      {
        virtual ~HandlerBaseImpl() = default;

        virtual R0 invoke(Args0...) = 0;

        virtual const std::type_info &type() const = 0;
      };

      template<class R0, class... Args0, class... SRest>
      struct HandlerBaseImpl<R0(Args0...), SRest...>
        : HandlerBaseImpl<SRest...>
      {
        using HandlerBaseImpl<SRest...>::invoke;
        virtual R0 invoke(Args0...) = 0;
      };

      template<class... Signatures>
      struct HandlerBase
        : HandlerBaseImpl<Signatures...>
      {
        virtual std::unique_ptr<HandlerBase> clone() const = 0;
      };


      // Handler template implementation
      template<class F, class AllSignatures, class... RemainingSignatures>
      class HandlerImpl;

      template<class F, class... AllSignatures>
      class HandlerImpl<F, std::tuple<AllSignatures...> >
        : public HandlerBase<AllSignatures...>
      {
      protected:
        F func_;

      public:
        HandlerImpl(F &&func) : func_(std::move(func)) {}

        const std::type_info &type() const final override
        {
          return typeid(F);
        }

        F &get() { return func_; }
      };

      template<class F, class AllSignatures,
               class R0, class... Args0, class... SRest>
      class HandlerImpl<F, AllSignatures, R0(Args0...), SRest...>
        : public HandlerImpl<F, AllSignatures, SRest...>
      {
        using Base = HandlerImpl<F, AllSignatures, SRest...>;

      public:
        using Base::Base;

        using Base::invoke;
        R0 invoke(Args0... args) final override
        {
          return this->func_(std::forward<Args0>(args)...);
        }
      };

      template<class F, class... Signatures>
      class Handler
        : public HandlerImpl<F, std::tuple<Signatures...>, Signatures...>
      {
        using Base = HandlerImpl<F, std::tuple<Signatures...>, Signatures...>;

      public:
        using Base::Base;

        std::unique_ptr<HandlerBase<Signatures...> >
        clone() const final override
        {
          return std::make_unique<Handler>(*this);
        }
      };

    } // namespace MultiFunctionImpl

    // function implementation
    template<class... Signatures>
    class MultiFunction
    {
      using HandlerBase = MultiFunctionImpl::HandlerBase<Signatures...>;
      template<class F>
      using Handler = MultiFunctionImpl::Handler<F, Signatures...>;

      std::unique_ptr<HandlerBase> target_;

    public:
      // construction
      MultiFunction() noexcept = default;
      MultiFunction(std::nullptr_t) noexcept : MultiFunction() {}

      MultiFunction(const MultiFunction &o) :
        target_(o.target_ ? o.target_->clone() : nullptr)
      { }

      MultiFunction(MultiFunction &&o) = default;

      template<class R, class... Args>
      MultiFunction(R (*f)(Args...)) :
        target_(f ? new Handler<R (*)(Args...)>(f) : nullptr)
      { }

      // TODO: member pointers

      // TODO: copying from other instances of MultiFunction

      template<class F>
      MultiFunction(F f) :
        target_(new Handler<F>(std::move(f)))
      { }

      // assignment
      MultiFunction& operator=(const MultiFunction &o)
      {
        return *this = MultiFunction(o);
      }

      MultiFunction& operator=(MultiFunction &&o) = default;

      MultiFunction& operator=(std::nullptr_t)
      {
        target_ = nullptr;
        return *this;
      }

      template<class F>
      MultiFunction& operator=(F &&f)
      {
        return *this = MultiFunction(std::forward<F>(f));
      }

      // TODO: assignment from reference_wrapper

      // destruction
      ~MultiFunction() = default;

      // modifiers
      void swap(MultiFunction &o) noexcept
      {
        using std::swap;
        swap(target_, o.target_);
      }

      // capacity
      explicit operator bool() const noexcept { return bool(target_); }

      // invocation
      template<class... Args>
      decltype(auto) operator()(Args &&... args) const
      {
        if(!target_)
          throw std::bad_function_call();
        return target_->invoke(std::forward<Args>(args)...);
      }

      // target access
      const std::type_info &target_type() const noexcept
      {
        if(!target_)
          return typeid(void);
        else
          return target_->type();
      }

      // requires: T is callable for all signatures
      template<class T>
      T* target() noexcept
      {
        if(target_type() == typeid(T))
          return &static_cast<Handler<T>&>(*target_).get();
        else
          return nullptr;
      }

      // requires: T is callable for all signatures
      template<class T>
      const T* target() const noexcept
      {
        if(target_type() == typeid(T))
          return &static_cast<Handler<T>&>(*target_).get();
        else
          return nullptr;
      }

      // nullptr comparisons
      friend bool operator==(const MultiFunction& f, nullptr_t) noexcept
      {
        return !f;
      }
      friend bool operator==(nullptr_t, const MultiFunction& f) noexcept
      {
        return !f;
      }

      friend bool operator!=(const MultiFunction& f, nullptr_t) noexcept
      {
        return bool(f);
      }
      friend bool operator!=(nullptr_t, const MultiFunction& f) noexcept
      {
        return bool(f);
      }

      // algorithms
      friend void swap(MultiFunction &f1, MultiFunction &f2) noexcept
      {
        f1.swap(f2);
      }
    };

  } // namespace React
} // namespace Dune


#endif // DUNE_REACT_UTILITY_MULTIFUNCTION_HH
