// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_REACT_UTILITY_ADEPT_HH
#define DUNE_REACT_UTILITY_ADEPT_HH

#if !HAVE_ADEPT
#error adept used, but not found by configure or not enabled in CMakeLists.txt
#error (try add_dune_adept_flags() or dune_enable_all_packages())
#endif // !HAVE_ADEPT

#include <adept.h>

#include <dune/common/promotiontraits.hh>

/** @file
 *
 * \brief Compatibility header Adept: A fast automatic differentiation library
 *        for C++
 *
 * \see http://www.met.reading.ac.uk/clouds/adept/
 *
 * If you want to use Adept with Dune, include this header instead of directly
 * including `<adept.h>`.  Besides providing `AdeptADType`, this header
 * provides the necessary specializations if of certain Dune traits types, in
 * particular `PromotionTraits`.
 *
 * \warning
 * The result of expressions involving Adept types are Adept expression
 * templates.  Make sure you evaluate such expression templates before the end
 * of the full-expression they were created in, if you need their value,
 * otherwise you risk segmentation faults.  Evaluation can be done by
 * convertig them back to adept number types.
 *
 * Example:
 * \code
 * template<class MaybeADType>
 * MaybeADType f(MaybeADType v)
 * {
 *   auto term = 2 * f - 1;
 *   return term; // error: need to evaluate `term`, but the temporary for the
 *                // subexpression `2 * f` no longer exists
 * }
 * \endcode
 * Instead, specify the type of the variable explicitly:
 * \code
 * template<class MaybeADType>
 * MaybeADType f(MaybeADType v)
 * {
 *   MaybeADType term = 2 * f - 1;
 *   return term; // OK
 * }
 * \endcode
 */

namespace Dune {
  namespace React {

    namespace AdeptImpl {

      template<class Real>
      struct AdeptADType; // General template is undefined

      // define for the one type supported by adept
      template<>
      struct AdeptADType<adept::Real>
      {
        using type = adept::aReal;
      };

    } // namespace AdeptImpl

    //! get the adept AD type for the type `Real`
    /**
     * Adept supports only one type (`adept::Real`), which can be switched by
     * setting the define `ADEPT_FLOATING_POINT_TYPE`, and which defaults to
     * `double`.  This template yields adept's automatic differentiation type
     * `adept::aReal` when instantiated with `adept::Real`.  Instantiating
     * this template with any other type leads to a subtitution failure.
     *
     * Using this template instead of `adept::aReal` directly implicitly
     * asserts that the type given as the template argument matches
     * `adept::Real`, which is useful when using adept from templated
     * algorithms.  It also makes the algorithm easier adaptable to a future
     * version of adept that may support more than a single type.
     */
    template<class Real>
    using AdeptADType = typename AdeptImpl::AdeptADType<Real>::type;

  } // namespace React

  template<>
  struct PromotionTraits<adept::aReal, adept::Real>
  {
    using PromotedType = adept::aReal;
  };

  template<>
  struct PromotionTraits<adept::Real, adept::aReal>
  {
    using PromotedType = adept::aReal;
  };

} // namespace Dune


#endif // DUNE_REACT_UTILITY_ADEPT_HH
