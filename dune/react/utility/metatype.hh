#ifndef DUNE_REACT_UTILITY_METATYPE_HH
#define DUNE_REACT_UTILITY_METATYPE_HH

#include <tuple>

namespace Dune {
  namespace React {

    //! A type that refers to another type
    /**
     * Tuples of `MetaType<T...>` members can be used to iterate over types
     * with `Dune::Hybrid::forEach` without the need to pass around actual
     * values of type `T...`.
     */
    template<class T>
    struct MetaType {
      using type = T;
    };

    template<class... T>
    using MetaTuple = std::tuple<MetaType<T>...>;

  } // namespace React
} // namespace Dune

#endif // DUNE_REACT_UTILITY_METATYPE_HH
