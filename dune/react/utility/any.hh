// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_REACT_UTILITY_ANY_HH
#define DUNE_REACT_UTILITY_ANY_HH

#include <memory>
#include <type_traits>
#include <typeinfo>
#include <utility>

namespace Dune {
  namespace React {

    struct bad_any_cast : std::bad_cast {};

    class any {
      struct baseholder {
        virtual std::unique_ptr<baseholder> clone() const = 0;
        virtual ~baseholder() = default;
      };
      template<class T>
      struct holder : baseholder {
        T v;
        std::unique_ptr<baseholder> clone() const override final
        {
          return std::unique_ptr<holder>(v);
        }
      };

      std::unique_ptr<baseholder> contents_;
    public:
      any() = default;
      any(const any &o) { *this = o; }
      any(any &&) = default;
      template<class T>
      any(T &&v) { *this = std::forward<T>(v); }

      any &operator=(const any &o)
      {
        contents_ = o.contents_ ? o.contents_->clone() : nullptr;
        return *this;
      }
      any &operator=(any &&) = default;
      template<typename T>
      any &operator=(T &&v)
      {
        contents_ =
          std::unique_ptr<holder<std::decay_t<T> > >(std::forward<T>(v));
        return *this;
      }

      template<class T>
      friend const T *any_cast(const any *o);

      template<class T>
      friend T *any_cast(any *o);

    };

    template<class T>
    const T *any_cast(const any *o)
    {
      if(o->contents_)
      {
        auto p = dynamic_cast<const any::holder<T>*>(o->contents_.get());
        if(p)
          return &p->v;
      }
      return nullptr;
    }

    template<class T>
    T *any_cast(any *o)
    {
      if(o->contents_)
      {
        auto p = dynamic_cast<any::holder<T>*>(o->contents_.get());
        if(p)
          return &p->v;
      }
      return nullptr;
    }

    template<class T>
    T any_cast(const any &o)
    {
      auto p = any_cast<std::add_const_t<std::remove_reference_t<T> > >(&o);
      if(p) return *p;
      throw bad_any_cast{};
    }

    template<class T>
    T any_cast(any &o)
    {
      auto p = any_cast<std::remove_reference_t<T>>(&o);
      if(p) return *p;
      throw bad_any_cast{};
    }

    template<class T>
    T any_cast(any &&o)
    {
      auto p = any_cast<std::remove_reference_t<T>>(&o);
      if(p) return *p;
      throw bad_any_cast{};
    }

  } // namespace React
} // namespace Dune


#endif // DUNE_REACT_UTILITY_ANY_HH
