// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_REACT_UTILITY_ANNOTATION_HH
#define DUNE_REACT_UTILITY_ANNOTATION_HH

#include <tuple>
#include <type_traits>
#include <utility>

namespace Dune {
  namespace React {

    template<class T>
    class AnnotationStore {
      typename T::type v;

    public:
      template<class... Args>
      AnnotationStore(Args &&... args) : v(std::forward<Args>(args)...) {}
      template<class... Args>
      AnnotationStore &operator=(Args &&... args)
      {
        v = { std::forward<Args>(args)... };
      }

      typename T::type & get() & { return v; }
      const typename T::type & get() const & { return v; }
      typename T::type && get() && { return v; }
    };

    template<class F, class... Tags>
    class Annotation :
      public F
    {
      using Storage = std::tuple<AnnotationStore<Tags>...>;
      Storage annotations_;

    public:
      using F::F;

      Annotation(const F &f, const Storage &t) :
        F(f), annotations_(t) {}
      Annotation(const F &f, Storage &&t) :
        F(f), annotations_(std::move(t)) {}
      Annotation(F &&f, const Storage &t) :
        F(std::move(f)), annotations_(t) {}
      Annotation(F &&f, Storage &&t) :
        F(std::move(f)), annotations_(std::move(t)) {}

      friend Storage &getAnnotations(Annotation &f)
      { return f.annotations_; }
      friend const Storage &getAnnotations(const Annotation &f)
      { return f.annotations_; }
    };

    template<class Tag, class F>
    struct HasAnnotation : std::false_type {};

    template<class Tag, class F, class... Tags1, class... Tags2>
    struct HasAnnotation<Tag, Annotation<F, Tags1..., Tag, Tags2...> > :
      std::true_type
    { };

    template<class F>
    std::tuple<> &getAnnotations(F &) {
      static std::tuple<> dummy;
      return dummy;
    }

    template<class F>
    const std::tuple<> &getAnnotations(const F &) {
      static const std::tuple<> dummy;
      return dummy;
    }

    template<class F, class... Tags>
    const F &unannotated(const Annotation<F, Tags...> &f)
    {
      return f;
    }

    template<class F, class... Tags>
    F &unannotated(Annotation<F, Tags...> &f)
    {
      return f;
    }

    template<class F, class... Tags>
    F unannotated(Annotation<F, Tags...> &&f)
    {
      return static_cast<F&&>(f);
    }

    template<class F>
    F &&unannotated(F &&f)
    {
      return std::forward<F>(f);
    }

    template<class F, class Tag, class V>
    Annotation<std::decay_t<F>, Tag> annotate(F &&f, Tag, V &&v)
    {
      return { std::forward<F>(f),
          std::tuple<AnnotationStore<Tag> >(std::forward<V>(v)) };
    }

    template<class F, class Tag, class V>
    std::enable_if_t<HasAnnotation<Tag, F>::value, F>
    annotate(F f, Tag, V &&v)
    {
      std::get<AnnotationStore<Tag> >(getAnnotations(f)) = std::forward<V>(v);
      return f;
    }

    template<class F, class... Tags, class Tag, class V>
    std::enable_if_t<!HasAnnotation<Tag, Annotation<F, Tags...> >::value,
                     Annotation<F, Tags..., Tag> >
    annotate(Annotation<F, Tags...> &&f, Tag, V &&v)
    {
      return { unannotated(f),
          std::tuple_cat(std::move(getAnnotations(f)),
                         std::tuple<AnnotationStore<Tag> >(std::forward<V>(v)))
          };
    }

    template<class F, class... Tags, class Tag, class V>
    std::enable_if_t<!HasAnnotation<Tag, Annotation<F, Tags...> >::value,
                     Annotation<F, Tags..., Tag> >
    annotate(const Annotation<F, Tags...> &f, Tag, V &&v)
    {
      return { unannotated(f),
          std::tuple_cat(getAnnotations(f),
                         std::tuple<AnnotationStore<Tag> >(std::forward<V>(v)))
          };
    }

    template<class F, class Tag>
    std::enable_if_t<!HasAnnotation<Tag, F>::value,
                     typename Tag::type>
    getAnnotation(const F &f, Tag, typename Tag::type def)
    {
      return def;
    }

    template<class F, class Tag>
    std::enable_if_t<HasAnnotation<Tag, F>::value,
                     typename Tag::type>
    getAnnotation(const F &f, Tag, typename Tag::type def)
    {
      return annotation(f, Tag{});
    }

    template<class F, class Tag>
    decltype(auto) annotation(F &&f, Tag)
    {
      return std::get<AnnotationStore<Tag> >
        (getAnnotations(std::forward<F>(f))).get();
    }

  } // namespace React
} // namespace Dune


#endif // DUNE_REACT_UTILITY_ANNOTATION_HH
