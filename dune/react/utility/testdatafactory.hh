// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_REACT_UTILITY_TESTDATAFACTORY_HH
#define DUNE_REACT_UTILITY_TESTDATAFACTORY_HH

#include <list>
#include <utility>

#include <dune/react/utility/any.hh>

namespace Dune {
  namespace React {

    template<class Arg>
    struct TestValueFactory
    {
      static Arg get() { return {}; }
    };

    template<class Arg>
    Arg makeTestValue() { return TestValueFactory<Arg>::get(); }

    class TestDataFactory
    {
      std::list<any> history_;

      template<class Arg>
      struct Getter;

      template<class Signature>
      struct Caller;

    public:
      template<class Arg>
      Arg get()
      {
        return Getter<Arg>::get(history_);
      }

      template<class Signature, class F>
      decltype(auto) call(F &&f)
      {
        return Caller<Signature>::call(*this, std::forward<F>(f));
      }
    };

    template<class Arg>
    struct TestDataFactory::Getter
    {
      template<class History>
      static Arg get(History &history)
      {
        return makeTestValue<Arg>();
      }
    };

    template<class Arg>
    struct TestDataFactory::Getter<Arg&>
    {
      template<class History>
      static Arg &get(History &history)
      {
        history.emplace_back(makeTestValue<Arg>());
        return any_cast<Arg &>(history.back());
      }
    };

    template<class Arg>
    struct TestDataFactory::Getter<Arg&&>
    {
      template<class History>
      static Arg &&get(History &history)
      {
        history.emplace_back(makeTestValue<Arg>());
        return std::move(any_cast<Arg &>(history.back()));
      }
    };

    template<class R, class... Args>
    struct TestDataFactory::Caller<R(Args...)>
    {
      template<class F>
      static decltype(auto) call(TestDataFactory &factory, F &&f)
      {
        return std::forward<F>(f)(factory.get<Args>()...);
      }
    };

  } // namespace React
} // namespace Dune


#endif // DUNE_REACT_UTILITY_TESTDATAFACTORY_HH
