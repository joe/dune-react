#ifndef DUNE_REACT_EBERHARDPERMEABILITY_HH
#define DUNE_REACT_EBERHARDPERMEABILITY_HH

/** @file
 *
 * This has basically been copied with some major reformatting and refactoring
 * from dune-pdelab-howto/src/utility/permeability_generator.hh @
 * 219f2a1c7a343aa1fc48e933c32b4b707384c78d, which was originally introduced
 * into dune-pdelab-howto by Peter Bastian, and probably written by him, too.
 */

#include <cassert>
#include <cmath>
#include <valarray>

#include <dune/common/fvector.hh>

namespace Dune {
  namespace React {

    namespace EberhardPermeabilityGeneratorImpl {

      class Ran1 {
        // IM may *look* like INT_MAX for 32-bit numbers, but it is in fact
        // a modulo in a linear congruential rng that simply uses the range of
        // integers very well.  And sizeof(long) >= 4 is guaranteed by the
        // standard.  TODO: replace with std::minstd_rand0.
        static constexpr long IA = 16807;
        static constexpr long IM = 2147483647;
        // these two are used to implement (idum*IA) % IM without danger of
        // overflowing
        static constexpr long IQ = 127773;
        static constexpr long IR = 2836;

        // this mixes the numbers generated from the above lcr using a table,
        // not sure what this is good for
        static constexpr long NTAB = 32;
        static constexpr double NDIV = (1+(IM-1)/NTAB);

        // used to make a uniform distribution between 0.0 and 1.0 (I think),
        // should possibly be replaced by std::uniform_real_distribution
        static constexpr double AM = (1.0/IM);
        static constexpr double EPS = 1.2e-7;
        static constexpr double RNMX = (1.0-EPS);

        // used as internal state, was originally static
        long iy=0;
        long iv[NTAB];

        // used as internal state, was originally a parameter
        long idum;

      public:
        Ran1(long seed) :
          idum(seed)
        {
          // if seed is not < 0, we'll encounter undefined behaviour
          assert(seed < 0);
        }

        double operator()()
        {
          int j;
          long k;
          double temp;

          if(idum <= 0 || !iy)
          {
            // this is probably supposed to guard against INT_MIN (as well as
            // non-negative values), but in that case it is undefined
            // behaviour
            if(-idum < 1) idum = 1;
            else          idum = -idum;

            for(j=NTAB+7;j>=0;j--)
            {
              // generate random number using lcg
              k = idum/IQ;
              idum = IA*(idum-k*IQ) - IR*k;
              if(idum < 0) idum += IM;

              if(j < NTAB) iv[j] = idum;
            }

            iy = iv[0];
          }

          // generate random number using lcg
          k = idum/IQ;
          idum = IA*(idum-k*IQ)-IR*k;
          if(idum < 0) idum += IM;

          // not sure what this does, is this a sampling distribution, maybe?
          j = iy/NDIV;
          iy = iv[j];
          iv[j] = idum;

          // this, I'm pretty sure, is std::uniform_real_distribution with
          // default arguments
          if((temp=AM*iy) > RNMX) return RNMX;
          else                    return temp;
        }
      };

    } // namespace EberhardPermeabilityGeneratorImpl

    template<long dim>
    class EberhardPermeabilityGenerator
    {
    public:
      EberhardPermeabilityGenerator
      ( //vector of correlation lengths for d dimensions
        const Dune::FieldVector<double,dim> &corr_vec,
        double normal_variance = 1.0, double n_mean = 0.0,
        long num_of_modes = 1000,
        long seed = -1083 // must be negative
        )
        : number_of_modes(num_of_modes),
          normal_mean(n_mean),
          norm_factor(std::sqrt(2.0 * normal_variance / number_of_modes))
      {
        static constexpr double pi = std::acos(-1.0);
        EberhardPermeabilityGeneratorImpl::Ran1 ran1(seed);

        alpha.resize(number_of_modes);
        q_vec.resize(dim*number_of_modes);

        for (long n = 0; n < number_of_modes; ++n)
        {
          alpha[n] = 2.0 * pi * ran1();
        }
        for (long d = 0; d < dim; ++d)
        {
          for (long n = 0; n < number_of_modes; ++n)
          {
            q_vec[n*dim + d] = gasdev(ran1) / corr_vec[d];
          }
        }
      }

      template<typename RF>
      double eval (const Dune::FieldVector<RF,dim>& x) const
      {
        using std::cos;
        using std::exp;

        std::valarray<double> space_vec(dim);
        for (int i=0; i<dim; i++) space_vec[i] = x[i];
        double sum = 0.0, arg_of_cos = 0.0;
        for (long n = 0; n < number_of_modes; ++n, arg_of_cos = 0.0)
        {
          for (long d = 0; d < dim; ++d)
            arg_of_cos += q_vec[n*dim + d] * space_vec[d];
          sum += cos (arg_of_cos + alpha[n]);
        }
        return exp (normal_mean + norm_factor * sum);
      }

    private:
      template<class RndGen>
      double gasdev (RndGen &ran1) // Numerical Recipes
      {
        using std::log;
        using std::sqrt;

        double fac,rsq,v1,v2;
        if(iset == 0) {
          do {
            v1=2.0*ran1()-1.0;
            v2=2.0*ran1()-1.0;
            rsq=v1*v1+v2*v2;
          } while (rsq >= 1.0 || rsq == 0.0);

          fac=sqrt(-2.0*log(rsq)/rsq);
          gset=v1*fac;
          iset=1;
          return v2*fac;
        } else {
          iset=0;
          return gset;
        }
      }

      long number_of_modes;
      double normal_mean; // mean of the normal field
      double norm_factor;
      std::valarray<double> q_vec;
      std::valarray<double> alpha;

      // used as internal state of gasdev(), was originally static
      int iset = 0;
      double gset;
    };

  } // namespace React
} // namespace Dune

#endif // DUNE_REACT_EBERHARDPERMEABILITY_HH
