// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=8 sw=2 sts=2:
#ifndef DUNE_REACT_LOCALOPERATOR_ADEPTJACOBIAN_HH
#define DUNE_REACT_LOCALOPERATOR_ADEPTJACOBIAN_HH

#include <cstddef>
#include <limits>

#include <dune/pdelab/gridfunctionspace/localfunctionspacetags.hh>
#include <dune/pdelab/gridfunctionspace/localvector.hh>

#include <dune/react/utility/adept.hh>

namespace Dune {
  namespace React {

    ////////////////////////////////////////////////////////////////////////
    //
    //  Implementation of jacobian_*() in terms of alpha_*() with automatic
    //  differentiation (using adept)
    //

    //! Implement jacobian_volume() based on alpha_volume() using adept
    /**
     * Derive from this class to add automatic differentiation jacobian for
     * volume.  The derived class needs to implement alpha_volume(), and must
     * accept `adept::aReal` as the member types of input and output vectors.
     *
     * \tparam Imp Type of the derived class (CRTP-trick).
     */
    template<typename Imp>
    class AdeptJacobianVolume
    {
    public:
      //! compute local jacobian of the volume term
      template<typename EG, typename LFSU, typename X, typename LFSV,
               typename Jacobian>
      void jacobian_volume
      ( const EG& eg,
        const LFSU& lfsu, const X& x, const LFSV& lfsv,
        Jacobian& mat) const
      {
        using D = typename X::value_type;
        using ADD = AdeptADType<D>;
        using ADX = PDELab::LocalVector<ADD, PDELab::TrialSpaceTag,
                                        typename X::weight_type>;
        using R = typename Jacobian::value_type;
        using ADR = AdeptADType<R>;
        using ADY = PDELab::LocalVector<ADR, PDELab::TestSpaceTag,
                                        typename Jacobian::weight_type>;

        adept::Stack stack;

        ADX u(x.size());
        // initialize with NaN so there is a better change that we detect
        // access to parts of the input vector not covered by lfsu
        for(std::size_t j = 0; j < u.size(); ++j)
          u.base()[j].set_value(std::numeric_limits<D>::quiet_NaN());
        // copy values from x
        for(std::size_t j = 0; j < lfsu.size(); ++j)
          u(lfsu, j).set_value(x(lfsu, j));

        stack.new_recording();

        // run algorithm
        ADY v(mat.nrows(), R(0));
        auto vView = v.weightedAccumulationView(mat.weight());
        asImp().alpha_volume(eg, lfsu, u, lfsv, vView);

        // mark independent variables
        for(std::size_t j = 0; j < lfsu.size(); ++j)
          stack.independent(u(lfsu, j));

        // mark dependent variables
        for(std::size_t i = 0; i < lfsv.size(); ++i)
          stack.dependent(v(lfsv, i));

        // compute jacobian
        R jac[lfsu.size()*lfsv.size()];
        stack.jacobian(jac);

        // transfer jacobian into PDELab data structures
        for(std::size_t i = 0; i < lfsv.size(); ++i)
          for(std::size_t j = 0; j < lfsu.size(); ++j)
            mat.rawAccumulate(lfsv, i, lfsu, j, jac[lfsu.size()*i + j]);
      }

    private:
      Imp& asImp () { return static_cast<Imp &> (*this); }
      const Imp& asImp () const { return static_cast<const Imp &>(*this); }
    };

    //! Implement jacobian_boundary() based on alpha_boundary() using adept
    /**
     * Derive from this class to add automatic differentiation jacobian for
     * boundary.  The derived class needs to implement alpha_boundary(), and
     * must accept `adept::aReal` as the member types of input and output
     * vectors.
     *
     * \tparam Imp Type of the derived class (CRTP-trick).
     */
    template<typename Imp>
    class AdeptJacobianBoundary
    {
    public:
      //! compute local jacobian of the boundary term
      template<typename IG, typename LFSU, typename X, typename LFSV,
               typename Jacobian>
      void jacobian_boundary
      ( const IG& ig,
        const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
        Jacobian& mat_ss) const
      {
        using D = typename X::value_type;
        using ADD = AdeptADType<D>;
        using ADX = PDELab::LocalVector<ADD, PDELab::TrialSpaceTag,
                                        typename X::weight_type>;
        using R = typename Jacobian::value_type;
        using ADR = AdeptADType<R>;
        using ADY = PDELab::LocalVector<ADR, PDELab::TestSpaceTag,
                                        typename Jacobian::weight_type>;

        adept::Stack stack;

        ADX u_s(x_s.size());
        // initialize with NaN so there is a better change that we detect
        // access to parts of the input vector not covered by lfsu
        for(std::size_t j = 0; j < u_s.size(); ++j)
          u_s.base()[j].set_value(std::numeric_limits<D>::quiet_NaN());
        // copy values from x_s
        for(std::size_t j = 0; j < lfsu_s.size(); ++j)
          u_s(lfsu_s, j).set_value(x_s(lfsu_s, j));

        stack.new_recording();

        // run algorithm
        ADY v_s(mat_ss.nrows(), R(0));
        auto v_sView = v_s.weightedAccumulationView(mat_ss.weight());
        asImp().alpha_boundary(ig, lfsu_s, u_s, lfsv_s, v_sView);

        // mark independent variables
        for(std::size_t j = 0; j < lfsu_s.size(); ++j)
          stack.independent(u_s(lfsu_s, j));

        // mark dependent variables
        for(std::size_t i = 0; i < lfsv_s.size(); ++i)
          stack.dependent(v_s(lfsv_s, i));

        // compute jacobian
        R jac_ss[lfsu_s.size()*lfsv_s.size()];
        stack.jacobian(jac_ss);

        // transfer jacobian into PDELab data structures
        for(std::size_t i = 0; i < lfsv_s.size(); ++i)
          for(std::size_t j = 0; j < lfsu_s.size(); ++j)
            mat_ss.rawAccumulate(lfsv_s, i, lfsu_s, j,
                                 jac_ss[lfsu_s.size()*i + j]);
      }

    private:
      Imp& asImp () { return static_cast<Imp &> (*this); }
      const Imp& asImp () const { return static_cast<const Imp &>(*this); }
    };

  } // namespace React
} // namespace Dune

#endif // DUNE_REACT_LOCALOPERATOR_ADEPTJACOBIAN_HH
