// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_REACT_LOCALOPERATOR_REACTFEM_HH
#define DUNE_REACT_LOCALOPERATOR_REACTFEM_HH

#include <algorithm>
#include <array>
#include <cstddef>
#include <type_traits>
#include <vector>

#include <dune/common/fvector.hh>

#include <dune/pdelab/common/quadraturerules.hh>
#include <dune/pdelab/common/referenceelements.hh>
#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include <dune/pdelab/localoperator/flags.hh>
#include <dune/pdelab/localoperator/idefault.hh>
// #include <dune/pdelab/localoperator/numericaljacobianapply.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>

#include <dune/react/localoperator/adeptjacobian.hh>

namespace Dune {
  namespace React {

    //! \brief a local operator for solving the linear convection-diffusion
    //!        equation with standard FEM
    /**
     * \f{align*}{
     *   \nabla\cdot(-A(x) \nabla u + b(x) u)
     *                                    &=& f(u,x) \mbox{ in } \Omega,  \\
     *                                  u &=& g \mbox{ on } \partial\Omega_D \\
     *    (b(x) u - A(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_N \\
     *            -(A(x)\nabla u) \cdot n &=& o \mbox{ on } \partial\Omega_O
     * \f}
     *
     * \note This formulation is valid for velocity fields which are
     *       non-divergence free.
     * \note Outflow boundary conditions should only be set on the outflow
     *       boundary
     *
     * \tparam Param Model of ReactionModelProblem
     */
    template<typename Param, typename FiniteElementMap>
    class ReactionFEM :
      // public PDELab::NumericalJacobianApplyVolume
      //          <ReactionFEM<Param, FiniteElementMap> >,
      // public PDELab::NumericalJacobianApplyBoundary
      //          <ReactionFEM<Param, FiniteElementMap> >,
      public AdeptJacobianVolume<ReactionFEM<Param, FiniteElementMap> >,
      public AdeptJacobianBoundary<ReactionFEM<Param, FiniteElementMap> >,
      public PDELab::FullVolumePattern,
      public PDELab::LocalOperatorDefaultFlags,
      public PDELab::InstationaryLocalOperatorDefaultMethods
               <typename Param::Traits::RangeFieldType>
    {
    public:
      // pattern assembly flags
      enum { doPatternVolume = true };

      // residual assembly flags
      enum { doAlphaVolume = true };
      enum { doAlphaBoundary = true };

      ReactionFEM (Param& param_, int intorderadd_=0)
        : param(param_), intorderadd(intorderadd_)
      {
      }

      // volume integral depending on test and ansatz functions
      template<typename EG, typename LFSU, typename X, typename LFSV,
               typename R>
      void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                        const LFSV& lfsv, R& r) const
      {
        static_assert(std::is_same<LFSU, LFSV>::value,
                      "We assume Galerkin method (lfsu == lfsv)");
        static_assert
          (std::is_same<typename FiniteElementMap::Traits::FiniteElementType,
           typename LFSU::ChildType::Traits::FiniteElementType>::value,
           "FiniteElement type of local function space does not match "
           "FiniteElementType of configured FiniteElementMap");

        // Define types
        using size_type = typename LFSU::Traits::SizeType;

        // dimensions
        const int dim = EG::Entity::dimension;

        constexpr auto species = Param::species();

        using RF = typename FiniteElementMap::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType;
        // this may be some automatic differentiation type
        using ADRF = typename X::value_type;
        // using RF = ADRF;

        // Reference to cell
        const auto& cell = eg.entity();

        // Get geometry
        auto geo = eg.geometry();

        auto ref_el = referenceElement(geo);
        auto localcenter = ref_el.position(0,0);
        // evaluate diffusion tensor at cell center, assume it is constant
        // over elements
        auto tensor = param.A(cell,localcenter);

        // Initialize vectors outside for loop
        std::vector<Dune::FieldVector<RF,dim> > gradphi(lfsu.child(0).size());

        // Transformation matrix
        typename EG::Geometry::JacobianInverseTransposed jac;

        // loop over quadrature points
        auto intorder =
          intorderadd + 2*lfsu.child(0).finiteElement().localBasis().order();
        for (const auto& ip : quadratureRule(geo,intorder))
        {
          // evaluate basis functions
          auto& phi =
            cache.evaluateFunction(ip.position(),
                                   lfsu.child(0).finiteElement().localBasis());

          // evaluate u
          std::array<ADRF, species> u;
          for(std::size_t s = 0; s < species; ++s)
          {
            u[s] = 0;
            for (size_type i=0; i<lfsu.child(s).size(); i++)
              u[s] += x(lfsu.child(s),i)*phi[i];
          }

          // evaluate gradient of shape functions (we assume Galerkin method
          // lfsu=lfsv)
          auto& js =
            cache.evaluateJacobian(ip.position(),
                                   lfsu.child(0).finiteElement().localBasis());

          // transform gradients of shape functions to real element
          jac = geo.jacobianInverseTransposed(ip.position());
          for (size_type i=0; i<lfsu.child(0).size(); i++)
            jac.mv(js[i][0],gradphi[i]);

          // compute gradient of u
          std::array<Dune::FieldVector<ADRF,dim>, species> gradu;
          for(std::size_t s = 0; s < species; ++s)
          {
            gradu[s] = 0.0;
            for (size_type i=0; i<lfsu.child(s).size(); i++)
              gradu[s].axpy(x(lfsu.child(s),i),gradphi[i]);
          }

          // compute A * gradient of u
          std::array<Dune::FieldVector<ADRF,dim>, species> Agradu;
          for(std::size_t s = 0; s < species; ++s)
            tensor[s].mv(gradu[s],Agradu[s]);

          // evaluate velocity field, sink term and source term
          auto b = param.b(cell,ip.position());
          auto f = param.f(cell,ip.position(), u);

          // integrate (A grad u)*grad phi_i - u b*grad phi_i - f*phi_i
          auto factor = ip.weight() * geo.integrationElement(ip.position());
          for(std::size_t s = 0; s < species; ++s)
            for (size_type i=0; i<lfsu.child(s).size(); i++)
              r.accumulate(lfsu.child(s), i,
                           factor * ( Agradu[s]*gradphi[i]
                                      - u[s]*(b*gradphi[i])
                                      - f[s]*phi[i] ));
        }
      }

      // boundary integral
      template<typename IG, typename LFSU, typename X, typename LFSV,
               typename R>
      void alpha_boundary (const IG& ig, const LFSU& lfsu_s, const X& x_s,
                           const LFSV& lfsv_s, R& r_s) const
      {
        static_assert(std::is_same<LFSU, LFSV>::value,
                      "We assume Galerkin method (lfsu == lfsv)");
        static_assert
          (std::is_same<typename FiniteElementMap::Traits::FiniteElementType,
           typename LFSU::ChildType::Traits::FiniteElementType>::value,
           "FiniteElement type of local function space does not match "
           "FiniteElementType of configured FiniteElementMap");

        // Define types
        using size_type = typename LFSV::Traits::SizeType;
        using BCC = PDELab::ConvectionDiffusionBoundaryConditions;

        constexpr auto species = Param::species();

        // this may be some automatic differentiation type
        using ADRF = typename X::value_type;

        // Reference to the inside cell
        const auto& cell_inside = ig.inside();

        // Get geometry
        auto geo = ig.geometry();

        // Get geometry of intersection in local coordinates of cell_inside
        auto geo_in_inside = ig.geometryInInside();

        auto ref_el = referenceElement(geo_in_inside);
        auto local_face_center = ref_el.position(0,0);
        auto intersection = ig.intersection();

        // evaluate boundary condition type
        auto bctype = param.bctype(intersection,local_face_center);
        bool allDirichlet =
          std::all_of(bctype.begin(), bctype.end(),
                      [](auto t) { return t == BCC::Dirichlet; });

        // skip rest if we are solely on Dirichlet boundaries
        if(allDirichlet)
          return;

        bool someNeumann =
          std::any_of(bctype.begin(), bctype.end(),
                      [](auto t) { return t == BCC::Neumann; });
        bool someOutflow =
          std::any_of(bctype.begin(), bctype.end(),
                      [](auto t) { return t == BCC::Outflow; });

        // loop over quadrature points and integrate normal flux
        auto intorder =
          intorderadd + 2*lfsu_s.child(0).finiteElement().localBasis().order();
        for (const auto& ip : quadratureRule(geo,intorder))
        {
          // position of quadrature point in local coordinates of element
          auto local = geo_in_inside.global(ip.position());

          // evaluate shape functions (assume Galerkin method)
          auto& phi =
            cache.evaluateFunction(local, lfsu_s.child(0).finiteElement()
                                            .localBasis());

          if(someNeumann)
          {
            // evaluate flux boundary condition
            auto j = param.j(intersection,ip.position());

            // integrate j
            auto factor = ip.weight()*geo.integrationElement(ip.position());
            for(std::size_t s = 0; s < species; ++s)
              if(bctype[s] == BCC::Neumann)
              {
                auto sfactor = j[s] * factor;
                for (size_type i=0; i<lfsu_s.child(s).size(); i++)
                  r_s.accumulate(lfsu_s.child(s),i, phi[i][0]*sfactor);
              }
          }

          if(someOutflow)
          {
            // evaluate velocity field and outer unit normal
            auto b = param.b(cell_inside,local);
            auto n = ig.unitOuterNormal(ip.position());
            auto bn = b*n;
            auto factor = ip.weight()*geo.integrationElement(ip.position());

            // evaluate outflow boundary condition
            auto o = param.o(intersection,ip.position());

            for(std::size_t s = 0; s < species; ++s)
              if(bctype[s] == BCC::Outflow)
              {
                // evaluate u[s]
                ADRF us = 0;
                for(size_type i=0; i<lfsu_s.child(s).size(); i++)
                  us += x_s(lfsu_s.child(s),i)*phi[i];

                // integrate o
                // Can't use `auto sfactor = ...` here, see the warning in
                // dune/react/utility/adept.hh
                ADRF sfactor = ( bn*us + o[s]) * factor;
                for (size_type i=0; i<lfsu_s.child(s).size(); i++)
                  r_s.accumulate(lfsu_s.child(s),i, phi[i]*sfactor);
              }
          }
        }
      }

      //! set time in parameter class
      void setTime (double t)
      {
        param.setTime(t);
      }

    private:
      using LocalBasisType = typename FiniteElementMap::Traits::
        FiniteElementType::Traits::LocalBasisType;

      Param& param;
      int intorderadd;
      Dune::PDELab::LocalBasisCache<LocalBasisType> cache;
    };

  } // namespace React
} // namespace Dune

#endif // DUNE_REACT_LOCALOPERATOR_REACTFEM_HH
