// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_REACT_PARAMETER_HH
#define DUNE_REACT_PARAMETER_HH

#include <algorithm>
#include <array>
#include <cstddef>
#include <iterator>

#include <dune/common/fvector.hh>

#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/constraints/common/constraintsparameters.hh>
#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>


namespace Dune {
  namespace React {

    //! Parameter class for solving the linear convection-diffusion equation
    /**
     * A parameter class for the linear convection-diffusion equation
     * \f{align*}{
     *   -\nabla\cdot\{ A(x) \nabla u) + b(x)\cdot \nabla u \}
     *                                   &=& f(x,u) \mbox{ in } \Omega,  \\
     *                                 u &=& g      \mbox{ on } \partial\Omega_D \\
     *   (b(x,u) - A(x)\nabla u) \cdot n &=& j      \mbox{ on } \partial\Omega_N \\
     *           -(A(x)\nabla u) \cdot n &=& o      \mbox{ on } \partial\Omega_O
     * \f}
     *
     * \note This formulation is valid for velocity fields which are
     *       non-divergence free.
     * \note Outflow boundary conditions should only be set on the outflow
     *       boundary
     *
     * \tparam T a traits class defining the necessary types
     */
    template<typename GV, typename RF, std::size_t N>
    class ReactionParameterInterface
    {
      typedef PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

    public:
      typedef PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

      //! the number of species
      static constexpr std::size_t species()
      {
        return N;
      }

      //! tensor diffusion coefficient
      std::array<typename Traits::PermTensorType, N>
      A (const typename Traits::ElementType& e,
         const typename Traits::DomainType& x) const
      {
        std::array<typename Traits::PermTensorType, N> I;

        for(auto &Ic : I)
          for (std::size_t i=0; i<Traits::dimDomain; i++)
            for (std::size_t j=0; j<Traits::dimDomain; j++)
              Ic[i][j] = (i==j) ? 1 : 0;

        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b(const typename Traits::ElementType& e,
        const typename Traits::DomainType& x) const
      {
        return typename Traits::RangeType(0.0);
      }

      //! reaction/source/sink term
      std::array<typename Traits::RangeFieldType, N>
      f (const typename Traits::ElementType& e,
         const typename Traits::DomainType& x,
         const std::array<typename Traits::RangeFieldType, N> &u) const
      {
        std::array<typename Traits::RangeFieldType, N> F;
        std::fill(F.begin(), F.end(), 0.0);
        return F;
      }

      //! boundary condition type function
      std::array<BCType, N>
      bctype (const typename Traits::IntersectionType& is,
              const typename Traits::IntersectionDomainType& x) const
      {
        std::array<BCType, N> t;
        std::fill(t.begin(), t.end(),
                  PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet);
        return t;
      }

      //! Dirichlet boundary condition value
      /**
       * May be evaluated where some components are not on a Dirichlet
       * boundary, result may contain garbage values there.  But beware: is
       * also used to evaluate the initial conditions everywhere in the
       * domain.
       */
      std::array<typename Traits::RangeFieldType, N>
      g (const typename Traits::ElementType& e,
         const typename Traits::DomainType& x) const
      {
        std::array<typename Traits::RangeFieldType, N> G;
        std::fill(G.begin(), G.end(), e.geometry().global(x).two_norm());
        return G;
      }

      //! Neumann boundary condition
      /**
       * May be evaluated where some components are not on a Neumann boundary,
       * result may contain garbage values there.
       */
      std::array<typename Traits::RangeFieldType, N>
      j (const typename Traits::IntersectionType& is,
         const typename Traits::IntersectionDomainType& x) const
      {
        std::array<typename Traits::RangeFieldType, N> J;
        std::fill(J.begin(), J.end(), 0.0);
        return J;
      }

      //! outflow boundary condition
      /**
       * May be evaluated where some components are not on an outflow
       * boundary, result may contain garbage values there.
       */
      std::array<typename Traits::RangeFieldType, N>
      o (const typename Traits::IntersectionType& is,
         const typename Traits::IntersectionDomainType& x) const
      {
        std::array<typename Traits::RangeFieldType, N> O;
        std::fill(O.begin(), O.end(), 0.0);
        return O;
      }

      void setTime(typename Traits::RangeFieldType) {}
    };

    //! \brief Adapter that extracts boundary condition type function from
    //!        parameter class
    /**
     *
     * \tparam Params Model of ReactionParameterInterface
     */
    template<typename Params>
    class ReactionBoundaryConditionAdapter
      :
      public Dune::PDELab::FluxConstraintsParameters,
      public Dune::PDELab::DirichletConstraintsParameters   /*@\label{bcp:base}@*/
    {
      const Params& params_;
      std::size_t species_;

    public:

      ReactionBoundaryConditionAdapter(const Params& params,
                                       std::size_t species)
        : params_(params), species_(species)
      {}

      template<typename I>
      bool isDirichlet(const I & ig,
                       const Dune::FieldVector<typename I::ctype,
                                               I::dimension-1>& coord) const
      {
        return params_.bctype( ig.intersection(), coord )[species_]
          == PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
      }

      template<typename I>
      bool isNeumann(const I & ig,
                     const Dune::FieldVector<typename I::ctype,
                                             I::dimension-1>& coord) const
      {
        return !isDirichlet( ig, coord );
      }

    };

    //! \brief Adapter that extracts Dirichlet boundary conditions from
    //!        parameter class
    /**
     * \tparam Param  Model of ReactionParameterInterface
     */
    template<typename Param>
    class ReactionDirichletExtensionAdapter :
      public Dune::PDELab::GridFunctionBase
              <Dune::PDELab::GridFunctionTraits
                <typename Param::Traits::GridViewType,
                 typename Param::Traits::RangeFieldType,
                 Param::species(),
                 Dune::FieldVector<typename Param::Traits::RangeFieldType,
                                   Param::species()>
                 >,
               ReactionDirichletExtensionAdapter<Param> >
    {
    public:
      using Traits = Dune::PDELab::GridFunctionTraits
        <typename Param::Traits::GridViewType,
         typename Param::Traits::RangeFieldType,
         Param::species(),
         Dune::FieldVector<typename Param::Traits::RangeFieldType,
                           Param::species()> >;

      //! constructor
      ReactionDirichletExtensionAdapter
      ( const typename Traits::GridViewType& gv, Param& param) :
        gv_(gv), param_(param)
      {}

      //! \copydoc GridFunctionBase::evaluate()
      void evaluate (const typename Traits::ElementType& e,
                     const typename Traits::DomainType& x,
                     typename Traits::RangeType& y) const
      {
        using std::begin;
        using std::end;

        auto res = param_.g(e,x);
        std::copy(begin(res), end(res), begin(y));
      }

      const typename Traits::GridViewType& getGridView () const
      {
        return gv_;
      }

      void setTime(double time)
      {
        param_.setTime(time);
      }

    private:
      const typename Traits::GridViewType gv_;
      Param& param_;
    };

  } // namespace React
} // namespace Dune


#endif // DUNE_REACT_PARAMETER_HH
