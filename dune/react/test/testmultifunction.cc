// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <tuple>
#include <type_traits>
#include <typeinfo>
#include <vector>

#include <dune/common/hybridutilities.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/react/utility/metatype.hh>
#include <dune/react/utility/multifunction.hh>
#include <dune/react/utility/testdatafactory.hh>

template<class Signature>
struct SignatureResult;

template<class R, class... Args>
struct SignatureResult<R(Args...)>
{
  using type = R;
};

template<class Signature>
using SignatureResultT = typename SignatureResult<Signature>::type;

template<class MetaSignatures>
struct MultiFunctionFor;

template<class... MetaSignatures>
struct MultiFunctionFor<std::tuple<MetaSignatures...> >
{
  using type = Dune::React::MultiFunction<typename MetaSignatures::type...>;
};

class UnitTest
{
  bool good_ = true;

  Dune::React::TestDataFactory factory_;

  template<class Arg>
  Arg makeTestData()
  {
    return factory_.template get<Arg>();
  }

  void checkTrue(bool val, const char *expr)
  {
    if(!val) {
      good_ = false;
      std::cerr << "Error: Check \"" << expr << "\" failed." << std::endl;
    }
  }

#define CHECK_TRUE(expr) this->checkTrue((expr), #expr)

  void checkUnreachable(const char *msg)
  {
    good_ = false;
    std::cerr << "Error: " << msg << std::endl;
  }

public:
  template<class MetaFunctions, class MetaSignatures, class MF>
  void testEmpty(MF &&mf)
  {
    // capacity
    CHECK_TRUE(!mf);

    // invocation
    Dune::Hybrid::forEach(MetaSignatures{}, [&](auto metaSig) {
        using Signature = typename decltype(metaSig)::type;

        // Apparently g++-4.9.2 fails to find captured entities (mf, cmf) in
        // static asserts.  So alias them from within the lambda, and use the
        // aliases.
        const auto &cmf = mf;

        static_assert
          (std::is_convertible<decltype(factory_.call<Signature>(cmf)),
                               SignatureResultT<Signature> >::value,
           "Unexpected return type.");

        try {
          factory_.call<Signature>(cmf);
          this->checkUnreachable("Expected exception was not thrown");
        }
        catch(std::bad_function_call) { /* nothing */ }
        catch(...) {
          this->checkUnreachable("Unexpected exception was thrown");
        }

      });

    // target access
    CHECK_TRUE(mf.target_type() == typeid(void));
    Dune::Hybrid::forEach(MetaFunctions{}, [&](auto metaF) {
        using F = typename decltype(metaF)::type;

        // Apparently g++-4.9.2 fails to find captured entities (mf, cmf) in
        // static asserts.  So alias them from within the lambda, and use the
        // aliases.
        auto &mmf = mf;
        const auto &cmf = mf;

        static_assert
          (std::is_same<decltype(mmf.template target<F>()), F *>::value,
           "Unexpected result type of target()");
        static_assert
          (std::is_same<decltype(cmf.template target<F>()), const F *>::value,
           "Unexpected result type of target()");

        CHECK_TRUE(mmf.template target<F>() == nullptr);
        CHECK_TRUE(cmf.template target<F>() == nullptr);

      });

    // nullptr comparison
    CHECK_TRUE(mf == nullptr);
    CHECK_TRUE(nullptr == mf);
    CHECK_TRUE(!(mf != nullptr));
    CHECK_TRUE(!(nullptr != mf));
  }

  template<class Function, class MetaFunctions, class MetaSignatures, class MF>
  void testNonempty(MF &&mf)
  {
    // capacity
    CHECK_TRUE(bool(mf));

    // invocation
    Dune::Hybrid::forEach(MetaSignatures{}, [&](auto metaSig) {
        using Signature = typename decltype(metaSig)::type;

        // Apparently g++-4.9.2 fails to find captured entities (mf, cmf) in
        // static asserts.  So alias them from within the lambda, and use the
        // aliases.
        const auto &cmf = mf;

        static_assert
          (std::is_convertible<decltype(factory_.call<Signature>(cmf)),
                               SignatureResultT<Signature> >::value,
           "Unexpected return type.");
        try {
          factory_.call<Signature>(cmf);
        }
        catch(std::bad_function_call) {
          this->checkUnreachable("Unexpected exception was thrown");
        }
        catch(...) { /* nothing */ }
      });

    // target access
    CHECK_TRUE(mf.target_type() == typeid(Function));
    Dune::Hybrid::forEach(MetaFunctions{}, [&](auto metaF) {
        using F = typename decltype(metaF)::type;

        // Apparently g++-4.9.2 fails to find captured entities (mf, cmf) in
        // static asserts.  So alias them from within the lambda, and use the
        // aliases.
        auto &mmf = mf;
        const auto &cmf = mf;

        static_assert
          (std::is_same<decltype(mmf.template target<F>()), F *>::value,
           "Unexpected result type of target()");
        static_assert
          (std::is_same<decltype(cmf.template target<F>()), const F *>::value,
           "Unexpected result type of target()");

        if(std::is_same<Function, F>::value)
          CHECK_TRUE(mmf.template target<F>() != nullptr);
        else
          CHECK_TRUE(mmf.template target<F>() == nullptr);

        if(std::is_same<Function, F>::value)
          CHECK_TRUE(cmf.template target<F>() != nullptr);
        else
          CHECK_TRUE(cmf.template target<F>() == nullptr);
      });

    // nullptr comparison
    CHECK_TRUE(!(mf == nullptr));
    CHECK_TRUE(!(nullptr == mf));
    CHECK_TRUE(mf != nullptr);
    CHECK_TRUE(nullptr != mf);
  }

  template<class MetaFunctions, class MetaSignatures, class... Functions>
  void test(Functions &&... functions)
  {
    using MF = typename MultiFunctionFor<MetaSignatures>::type;
    using AllMetaFunctions =
      decltype(std::tuple_cat
               (Dune::React::MetaTuple<std::decay_t<Functions>...>{},
                MetaFunctions{}));

    MF mfempty;
    testEmpty<AllMetaFunctions, MetaSignatures>(mfempty);

    std::vector<MF> mf;
    mf.reserve(sizeof...(Functions));
    Dune::Hybrid::forEach(std::tie(functions...), [&] (auto &f) {
        using F = std::decay_t<decltype(f)>;
        mf.emplace_back(f);
        this->testNonempty<F, AllMetaFunctions, MetaSignatures>(mf.back());
      });
  }

  bool good() const { return good_; }

  #undef CHECK_TRUE
};

int main(int argc, char** argv)
{
  using Dune::React::MetaTuple;
  Dune::MPIHelper::instance(argc, argv);

  UnitTest t;

  auto min = [] (auto a, auto b)
    {
      using std::min;
      return min(a, b);
    };

  t.test<MetaTuple<double (*)(double, double)>,
         MetaTuple<int(int, int), double(double, double)> >
    (std::plus<>(), std::multiplies<>(), min);

  return t.good() ? EXIT_SUCCESS : EXIT_FAILURE;
}
