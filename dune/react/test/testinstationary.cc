// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <array>
#include <cmath>
#include <iostream>
#include <string>

#include <dune/common/array.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/io/file/vtk/vtksequencewriter.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/istl/operators.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solver.hh>
#include <dune/istl/solvers.hh>

#include <dune/pdelab/backend/interface.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/gridfunctionspace/powergridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/pdelab/gridoperator/onestep.hh>
#include <dune/pdelab/instationary/onestepparameter.hh>
#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include <dune/pdelab/localoperator/l2.hh>
#include <dune/pdelab/stationary/linearproblem.hh>

#include <dune/react/localoperator/reactfem.hh>
#include <dune/react/parameter.hh>

//! Example problem to test one-species instationary linear operation
/**
 * Domain is unit square: \f$ \Omega = (-1, 1)^2 \f$
 *
 * Diffusion tensor = 1e-3.
 *
 * Velocity field is circular donut-shaped with \f$ \mathbf b(x,y) = v(r)
 * \mathbf{\hat e}_\phi \f$, where \f$ x = r \cos\phi \f$, \f$ y = r \sin\phi
 * \f$ and
 * \f{equation}
 *   \mathbf{\hat e}_\phi = \begin{pmatrix}
 *     -\sin\phi \\
 *     \cos\phi
 *   \end{pmatrix}
 * \f}.
 * The velocity profile is quadratic in the range \f$ r \in [0, 1] \f$ with
 * \f$ \max_r f(r) = 1 \f$ and zero at the ends of the range, and it is also
 * zero for \f$ r > 1 \f$:
 * \f{equation*}
 *    v(r) = \begin{cases}
 *      -4r^2 + 4r & r \leq 1 \\
 *      0          & r \geq 1
 *    \end{cases}
 * \f}
 *
 * Boundary conditions are homogeneous dirichlet everywhere.
 *
 * Initial conditions are 0
 *
 * Source/sink/reaction is
 * \f{equation}
 *   G(0.2, r-0.45) G(\pi/16, \phi) - G(0.1, r-0.5) u
 * \f}
 * where \f$ G(\sigma, x) \f$ is the Gaussian of width \f$\sigma\f$ around 0,
 * \f$ r=\sqrt{x^2+y^2} \f$ is the distance to the center and \f$ \phi = \atan
 * \frac x y \f$ is the angular coordinate, and \f$ u \f$ is the solution.
 */

template<typename GV, typename RF>
class InstationaryModelProblem :
  public Dune::React::ReactionParameterInterface<GV, RF, 1>
{
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;
  using Interface = Dune::React::ReactionParameterInterface<GV, RF, 1>;

  template<class T>
  static T gaussian(T sigma, T x)
  {
    using std::acos;
    using std::exp;
    using std::sqrt;

    constexpr auto sqrt2pi = sqrt(2*acos(T(-1)));
    auto r = x/sigma;
    return exp(-0.5*r*r)/(sigma*sqrt2pi);
  }

public:
  using Traits = typename Interface::Traits;

  //! tensor diffusion coefficient
  std::array<typename Traits::PermTensorType, 1>
  A (const typename Traits::ElementType& e,
     const typename Traits::DomainType& x) const
  {
    std::array<typename Traits::PermTensorType, 1> I{ 0 };

    for (std::size_t i=0; i<Traits::dimDomain; i++)
      I[0][i][i] = 1e-2;

    return I;
  }

  //! velocity field
  typename Traits::RangeType
  b(const typename Traits::ElementType& e,
    const typename Traits::DomainType& x) const
  {
    auto xglobal = e.geometry().global(x);
    auto r = xglobal.two_norm();

    auto vr = r < 1 ? 4*r*(1-r) : 0;
    typename Traits::RangeType result{ -xglobal[1], xglobal[0] };
    // r == 0 can only happen for x==y==0, in which case the final result
    // should be 0 anyway
    if(r > 0) result /= r;
    // now result == e_phi
    result *= vr;

    return result;
  }

  //! reaction/source/sink term
  template<class ADRF>
  std::array<ADRF, 1>
  f (const typename Traits::ElementType& e,
     const typename Traits::DomainType& x,
     const std::array<ADRF, 1> &u) const
  {
    using std::acos;
    using std::atan2;

    constexpr auto pi = acos(-1.0);
    auto xglobal = e.geometry().global(x);
    auto r = xglobal.two_norm();
    auto phi = atan2(xglobal[1], xglobal[0]);

    return { gaussian(0.2, r-0.45) * gaussian(pi/16, phi)
        - gaussian(0.1, r-0.5) * u[0] };
  }

  //! Dirichlet boundary condition value
  std::array<typename Traits::RangeFieldType, 1>
  g(const typename Traits::ElementType& e,
    const typename Traits::DomainType& x) const
  {
    // auto xglobal = e.geometry().global(x);

    // if(-0.5 < xglobal[0] && xglobal[0] < 0.5 &&
    //    0.0 < xglobal[1] && xglobal[1] < 0.75)
    //   return { 1 };
    // else
      return { 0 };
  }

  //! set time for subsequent evaluation
  void setTime (RF t)
  {
    time = t;
  }

private:
  RF time = 0;
  RF kx = 3.0;
  RF ky = 3.0;
};

//===============================================================
// Problem setup and solution
//===============================================================

// generate a P1 function and output it
template<typename CON, typename GV, typename FEM>
void do_simulation (const GV& gv, const FEM& fem, const std::size_t degree,
                    double dt, double endOfTime, std::string filename)
{
  using Dune::PDELab::Backend::native;

  // constants and types
  typedef typename FEM::Traits::FiniteElementType::Traits::
    LocalBasisType::Traits::RangeFieldType R;

  // make function space
  using VB = Dune::PDELab::ISTL::VectorBackend<>;
  using GFS0 = Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VB>;
  GFS0 gfs0(gv,fem);
  gfs0.name("solution");
  using GFS = Dune::PDELab::PowerGridFunctionSpace<GFS0, 1, VB>;
  GFS gfs(gfs0);

  // make model problem
  typedef InstationaryModelProblem<GV,R> Problem;
  Problem problem;

  // make constraints map and initialize it from a function
  typedef typename GFS::template ConstraintsContainer<R>::Type C;
  C cg;
  cg.clear();
  using BC0 = Dune::React::ReactionBoundaryConditionAdapter<Problem>;
  BC0 bctype0(problem, 0);
  using BC = Dune::PDELab::PowerConstraintsParameters<BC0, 1>;
  BC bctype(bctype0);
  Dune::PDELab::constraints(bctype,gfs,cg);

  // make local operator
  using LOP =  Dune::React::ReactionFEM<Problem,FEM>;
  LOP lop(problem);
  typedef Dune::PDELab::PowerL2 MLOP;
  MLOP mlop(2*degree+2);

  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  // 27 is too large / correct for all test cases, so should work fine
  MBE mbe(27);

  //Dune::PDELab::FractionalStepParameter<Real> method;
  Dune::PDELab::Alexander2Parameter<R> method;

  // make grid operator
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,
                                     MBE,
                                     double,double,double,
                                     C,C> GO0;
  GO0 go0(gfs,cg,gfs,cg,lop,mbe);
  using GO1 = Dune::PDELab::GridOperator<GFS,GFS,MLOP,MBE,R,R,R,C,C>;
  GO1 go1(gfs,cg,gfs,cg,mlop,mbe);
  typedef Dune::PDELab::OneStepGridOperator<GO0,GO1> IGO;
  IGO igo(go0,go1);

  // make coefficent Vector and initialize it from a function
  typedef typename IGO::Traits::Domain DV;
  DV x(gfs, 0.0);

  // initialize DOFs from Dirichlet extension
  typedef Dune::React::ReactionDirichletExtensionAdapter<Problem> G;
  G g(gv,problem);
  problem.setTime(0.0);
  Dune::PDELab::interpolate(g,gfs,x);

  // linear problem solver
  typedef Dune::PDELab::ISTLBackend_OVLP_BCGS_ILU0<GFS, C> LS;
  LS ls(gfs, cg);

  typedef Dune::PDELab::StationaryLinearProblemSolver<IGO,LS,DV> PDESOLVER;
  PDESOLVER pdesolver(igo,ls,1e-10);

  Dune::PDELab::OneStepMethod<R,IGO,PDESOLVER,DV,DV>
    osm(method,igo,pdesolver);
  osm.setVerbosityLevel(2);

  auto stationaryVTKWriter =
    std::make_shared<Dune::VTKWriter<GV> >(gv,Dune::VTK::conforming);
  Dune::VTKSequenceWriter<GV> vtkwriter(stationaryVTKWriter,filename,"","");
  using VelocityGF =
    Dune::PDELab::ConvectionDiffusionVelocityExtensionAdapter<Problem>;
  VelocityGF velocityGF(gv, problem);
  vtkwriter.addVertexData
    (std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<VelocityGF> >
      (velocityGF, "velocity"));
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,x);

  // time loop
  R time = 0.0;
  vtkwriter.write(time,Dune::VTK::appendedraw);
  while (time<endOfTime-1e-10)
  {
    problem.setTime(time+dt);

    // do time step
    DV xnew(gfs,0.0);
    osm.apply(time,dt,x,xnew);

    // accept time step
    x = xnew;
    time += dt;

    // VTK output
    vtkwriter.write(time,Dune::VTK::appendedraw);
  }
}

//===============================================================
// Main program with grid setup
//===============================================================

int main(int argc, char** argv)
{
  try{
    // initialize MPI, finalize is done automatically on exit
    Dune::MPIHelper::instance(argc,argv);

    double T = 10;
    int cells = 1;
    int refines = 4;
    auto h = 2.0/cells/(1<<refines);
    double dt = h/2;

    // YaspGrid Q1 2D test
    {
      // make grid
      using Grid =
        Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<double,2> >;
      Grid grid({-1.0,-1.0}, {1.0,1.0}, {cells,cells});
      grid.globalRefine(refines);
      grid.loadBalance();

      // get view
      using GV = Grid::LeafGridView;
      const GV& gv=grid.leafGridView();

      // make finite element map
      typedef GV::Grid::ctype DF;
      typedef Dune::PDELab::QkLocalFiniteElementMap<GV,DF,double,1> FEM;
      FEM fem(gv);

      // solve problem
      do_simulation<Dune::PDELab::ConformingDirichletConstraints>
        (gv, fem, 1, dt, T, "instationary_yasp_Q1_2d");
    }
  }
  catch (std::exception & e) {
    std::cout << "Exception: " << e.what() << std::endl;
    throw;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }
}
