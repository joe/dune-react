// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <typeinfo>

#include <dune/common/classname.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/onedgrid.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/react/config/structuredgrid.hh>

class UnitTest
{
  bool good_ = true;

  void assertType(const std::type_info &expectedInfo,
                  const std::string &expectedName, const char *config)
  {
    Dune::ParameterTree p;
    std::istringstream stream(config);
    Dune::ParameterTreeParser::readINITree(stream, p);

    Dune::React::withStructuredGrid<
      Dune::YaspGrid<1>,
      Dune::YaspGrid<2>,
      Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<double, 2> >,
      Dune::YaspGrid<3>,
      Dune::OneDGrid
      >(p, [&](auto &g) {
        if(typeid(g) != expectedInfo)
        {
          std::cerr << "Error: expected type '" << expectedName
                    << "', but got type '" << Dune::className(g)
                    << "' for the following configuration:\n"
                    << "=======================================\n"
                    << config
                    << "=======================================" << std::endl;
          good_ = false;
        }
      });
  }

public:
  bool good() const { return good_; }

  template<class Expected>
  void assertType(const char *config)
  {
    assertType(typeid(Expected), Dune::className<Expected>(), config);
  }
};

int main(int argc, char** argv)
{
  using Dune::EquidistantOffsetCoordinates;
  using Dune::OneDGrid;
  using Dune::YaspGrid;

  Dune::MPIHelper::instance(argc, argv);

  UnitTest t;

  t.assertType<YaspGrid<1> >("");
  t.assertType<OneDGrid    >("elemtype = simplex\n");
  t.assertType<OneDGrid    >("manager = OneDGrid\n");
  t.assertType<YaspGrid<2> >("lower = 0 0\n");
  t.assertType<YaspGrid<2, EquidistantOffsetCoordinates<double, 2> > >
                            ("lower = -1 -1\n");
  t.assertType<YaspGrid<3> >("dimension = 3\n");

  return t.good() ? EXIT_SUCCESS : EXIT_FAILURE;
}
