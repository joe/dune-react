// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <array>
#include <cmath>
#include <iostream>
#include <string>

#include <dune/common/array.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/io/file/vtk/vtksequencewriter.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/istl/operators.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solver.hh>
#include <dune/istl/solvers.hh>

#include <dune/pdelab/backend/interface.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/gridfunctionspace/powergridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/pdelab/gridoperator/onestep.hh>
#include <dune/pdelab/instationary/onestepparameter.hh>
#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include <dune/pdelab/localoperator/l2.hh>
#include <dune/pdelab/newton/newton.hh>

#include <dune/react/eberhardpermeability.hh>
#include <dune/react/localoperator/reactfem.hh>
#include <dune/react/parameter.hh>

//! Example problem to test one-species instationary linear operation
/**
 * Inspired by the old dune-course nonlinear exercises.
 *
 * 2 species.
 *
 * Domain is square: \f$ \Omega = (0, 2)^2 \f$
 */

template<class RF>
struct ProblemParams
{
  RF d0;
  RF d1;
  RF tau;
  RF sigma;
  RF lambda;
  RF kappa;
};

template<typename GV, typename RF>
class InstationaryModelProblem :
  public Dune::React::ReactionParameterInterface<GV, RF, 2>
{
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;
  using Interface = Dune::React::ReactionParameterInterface<GV, RF, 2>;

public:
  using Traits = typename Interface::Traits;

  InstationaryModelProblem(const ProblemParams<RF> &params) :
    d0(params.d0), d1(params.d1), lambda(params.lambda), kappa(params.kappa),
    sigma(params.sigma)
  {
    using std::abs;

    // we're dividing by tau, make sure that is legal
    assert(abs(params.tau) > std::numeric_limits<RF>::epsilon()*8);
    tauinv = 1/params.tau;
  }

  //! tensor diffusion coefficient
  std::array<typename Traits::PermTensorType, 2>
  A (const typename Traits::ElementType& e,
     const typename Traits::DomainType& x) const
  {
    std::array<typename Traits::PermTensorType, 2> I{ 0 };

    for (std::size_t i=0; i<Traits::dimDomain; i++)
      I[0][i][i] = d0;
    for (std::size_t i=0; i<Traits::dimDomain; i++)
      I[1][i][i] = d1*tauinv;

    return I;
  }

  //! reaction/source/sink term
  template<class ADRF>
  std::array<ADRF, 2>
  f (const typename Traits::ElementType& e,
     const typename Traits::DomainType& x,
     const std::array<ADRF, 2> &u) const
  {
    // Can't use `auto f0 = ...` here, see the warning in
    // dune/react/utility/adept.hh
    ADRF f0 = lambda*u[0] - u[0]*u[0]*u[0] - kappa;

    return { f0 - sigma*u[1], (u[0] - u[1])*tauinv };
  }

  //! boundary condition type function
  std::array<BCType, 2>
  bctype (const typename Traits::IntersectionType& is,
          const typename Traits::IntersectionDomainType& x) const
  {
    std::array<BCType, 2> t;
    std::fill(t.begin(), t.end(),
              Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann);
    return t;
  }

  //! Dirichlet boundary/initial condition value
  std::array<typename Traits::RangeFieldType, 2>
  g(const typename Traits::ElementType& e,
    const typename Traits::DomainType& x) const
  {
    auto xglobal = e.geometry().global(x);
    return {
      log10(field[0].eval(xglobal)),
      log10(field[1].eval(xglobal))
    };
  }

private:
  RF d0;
  RF d1;
  RF tauinv;
  RF lambda;
  RF kappa;
  RF sigma;

  using CorrLen = Dune::FieldVector<double, GV::dimensionworld>;
  Dune::React::EberhardPermeabilityGenerator<GV::dimensionworld> field[2] = {
    { CorrLen(2.0/100.0), 1, 0.0, 5000, -1083 },
    { CorrLen(2.0/100.0), 1, 0.0, 5000, -34   },
  };
};

//===============================================================
// Problem setup and solution
//===============================================================

// generate a P1 function and output it
template<typename CON, typename Params, typename GV, typename FEM>
void do_simulation (Params params, const GV& gv, const FEM& fem,
                    const std::size_t degree, double dt, double endOfTime,
                    std::string filename)
{
  using Dune::PDELab::Backend::native;

  // constants and types
  typedef typename FEM::Traits::FiniteElementType::Traits::
    LocalBasisType::Traits::RangeFieldType R;
  static_assert(std::is_same<ProblemParams<R>, Params>::value,
                "Param class instantiaten with wrong type");

  // make function space
  using VB = Dune::PDELab::ISTL::VectorBackend<>;
  using GFS0 = Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VB>;
  GFS0 gfs0(gv,fem);
  using GFS = Dune::PDELab::PowerGridFunctionSpace<GFS0, 2, VB>;
  GFS gfs(gfs0);

  // make model problem
  typedef InstationaryModelProblem<GV,R> Problem;
  Problem problem(params);

  // make constraints map and initialize it from a function
  typedef typename GFS::template ConstraintsContainer<R>::Type C;
  C cg;
  cg.clear();
  using BC0 = Dune::React::ReactionBoundaryConditionAdapter<Problem>;
  BC0 bctype0(problem, 0);
  BC0 bctype1(problem, 1);
  using BC = Dune::PDELab::PowerConstraintsParameters<BC0, 2>;
  BC bctype(bctype0, bctype1);
  Dune::PDELab::constraints(bctype,gfs,cg);

  // make local operator
  using LOP =  Dune::React::ReactionFEM<Problem,FEM>;
  LOP lop(problem);
  typedef Dune::PDELab::PowerL2 MLOP;
  MLOP mlop(2*degree+2);

  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  // 27 is too large / correct for all test cases, so should work fine
  MBE mbe(27);

  // make grid operator
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,R,R,R,C,C> GO0;
  GO0 go0(gfs,cg,gfs,cg,lop,mbe);
  using GO1 = Dune::PDELab::GridOperator<GFS,GFS,MLOP,MBE,R,R,R,C,C>;
  GO1 go1(gfs,cg,gfs,cg,mlop,mbe);
  typedef Dune::PDELab::OneStepGridOperator<GO0,GO1> IGO;
  IGO igo(go0,go1);

  // make coefficent Vector and initialize it from a function
  typedef typename IGO::Traits::Domain DV;
  DV x(gfs, 0.0);

  // initialize DOFs from Dirichlet extension
  typedef Dune::React::ReactionDirichletExtensionAdapter<Problem> G;
  G g(gv,problem);
  problem.setTime(0.0);
  Dune::PDELab::interpolate(g,gfs,x);

  // linear problem solver
  typedef Dune::PDELab::ISTLBackend_OVLP_BCGS_ILU0<GFS, C> LS;
  LS ls(gfs, cg);

  typedef Dune::PDELab::Newton<IGO,LS,DV> PDESOLVER;
  PDESOLVER pdesolver(igo,ls);
  pdesolver.setReassembleThreshold(0.0);
  pdesolver.setVerbosityLevel(2);
  pdesolver.setReduction(1e-10);
  pdesolver.setMinLinearReduction(1e-4);
  pdesolver.setMaxIterations(25);
  pdesolver.setLineSearchMaxIterations(10);

  //Dune::PDELab::FractionalStepParameter<Real> method;
  Dune::PDELab::Alexander2Parameter<R> method;

  Dune::PDELab::OneStepMethod<R,IGO,PDESOLVER,DV,DV>
    osm(method,igo,pdesolver);
  osm.setVerbosityLevel(2);

  auto stationaryVTKWriter =
    std::make_shared<Dune::VTKWriter<GV> >(gv,Dune::VTK::conforming);
  Dune::VTKSequenceWriter<GV> vtkwriter(stationaryVTKWriter,filename,"","");
  using VelocityGF =
    Dune::PDELab::ConvectionDiffusionVelocityExtensionAdapter<Problem>;
  VelocityGF velocityGF(gv, problem);
  vtkwriter.addVertexData
    (std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<VelocityGF> >
      (velocityGF, "velocity"));
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,x,
                  Dune::PDELab::vtk::defaultNameScheme().prefix("solution"));

  // time loop
  R time = 0.0;
  vtkwriter.write(time,Dune::VTK::appendedraw);
  while (time<endOfTime-1e-10)
  {
    problem.setTime(time+dt);

    // do time step
    DV xnew(gfs,0.0);
    osm.apply(time,dt,x,xnew);

    // accept time step
    x = xnew;
    time += dt;

    // VTK output
    vtkwriter.write(time,Dune::VTK::appendedraw);
  }
}

//===============================================================
// Main program with grid setup
//===============================================================

int main(int argc, char** argv)
{
  try{
    // initialize MPI, finalize is done automatically on exit
    Dune::MPIHelper::instance(argc,argv);

    double T = 1;
    int refines = 4;
    auto h = 2.0/(1<<refines);
    double dt = h/2;

    // YaspGrid Q1 2D test
    {
      // make grid
      using Grid = Dune::YaspGrid<2>;
      Grid grid({1.0,1.0}, {1,1});
      grid.globalRefine(refines);
      grid.loadBalance();

      // get view
      using GV = Grid::LeafGridView;
      const GV& gv=grid.leafGridView();

      // make finite element map
      typedef GV::Grid::ctype DF;
      typedef Dune::PDELab::QkLocalFiniteElementMap<GV,DF,double,1> FEM;
      FEM fem(gv);

      ProblemParams<double> params = {
        0.000964, // RF d0;
        0.0001,   // RF d1;
        4.0,      // RF tau;
        1.0,      // RF sigma;
        0.9,      // RF lambda;
        0.0,      // RF kappa;
      };

      // solve problem
      do_simulation<Dune::PDELab::ConformingDirichletConstraints>
        (params, gv, fem, 1, dt, T, "woodstock_yasp_Q1_2d");
    }
  }
  catch (std::exception & e) {
    std::cout << "Exception: " << e.what() << std::endl;
    throw;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }
}
