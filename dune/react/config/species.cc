#include <config.h>

#include <algorithm>
#include <cstddef>
#include <ostream>
#include <string>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>

#include <dune/react/config/species.hh>

namespace Dune {
  namespace React {

    SpeciesMap::SpeciesMap(const Dune::ParameterTree &config) :
      names_(config.get<std::vector<std::string> >("names"))
    {
      if(names_.size() == 0)
        DUNE_THROW(Exception, "List of species names is empty");
    }

    std::size_t SpeciesMap::index(const std::string &name) const
    {
      auto begin = names_.begin();
      auto end = names_.end();
      auto pos = std::find(begin, end, name);
      if(pos == end)
        DUNE_THROW(Exception,
                   "\"" << name << "\" is not the name of a species");
      return pos - begin;
    }

    const std::string &SpeciesMap::name(std::size_t index) const
    {
      if(index >= names_.size())
        DUNE_THROW(Exception, index << " is not a valid species index "
                   << "(valid indices range from 0 to " << names_.size() - 1
                   << ", inclusive)");
      return names_[index];
    }

    std::ostream &operator<<(std::ostream &str, const SpeciesMap &map)
    {
      const char *sep = "";
      for(std::size_t i = 0; i < map.size(); ++i)
      {
        str << sep << i << "=" << map.name(i);
        sep = ", ";
      }

      return str;
    }

  } // namespace React
} // namespace Dune
