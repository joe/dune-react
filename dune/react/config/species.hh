#ifndef DUNE_REACT_CONFIG_SPECIES_HH
#define DUNE_REACT_CONFIG_SPECIES_HH

#include <cstddef>
#include <ostream>
#include <string>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>

namespace Dune {
  namespace React {

    class SpeciesMap
    {
      std::vector<std::string> names_;

    public:
      //! construct from ParameterTree
      /**
       * There should be a single key 'names' listing the names of the
       * species, in-order.
       */
      SpeciesMap(const Dune::ParameterTree &config);

      //! convert species name into species index
      std::size_t        index(const std::string &name) const;
      //! convert species index into name
      const std::string &name(std::size_t index) const;
      //! get number of species
      std::size_t        size() const { return names_.size(); }

      friend std::ostream &operator<<(std::ostream &, const SpeciesMap &);
    };

  } // namespace React
} // namespace Dune

#endif // DUNE_REACT_CONFIG_SPECIES_HH
