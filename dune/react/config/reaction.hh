// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_REACT_CONFIG_REACTION_HH
#define DUNE_REACT_CONFIG_REACTION_HH

#include <algorithm>
#include <iterator>
#include <string>
#include <type_traits>
#include <utility>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/react/config/species.hh>
#include <dune/react/utility/multifunction.hh>

namespace Dune {
  namespace React {

    //! create are reaction term from a parameter tree
    /**
     * \tparam U0    Type of test vector.  The element type of this vector is
     *               used to read some configuration values from the parameter
     *               tree.  It is also used to construct the first supported
     *               call signature.
     * \tparam Un... Alternate test vector types.  These can have
     *               e.g. automatic differentiation types as element types.
     *               They are used to construct additional supported call
     *               signatures.
     */
    template<class U0, class... Un, class GV>
    MultiFunction<U0(const typename GV::template Codim<0>::Entity &,
                     const FieldVector<typename GV::ctype, GV::dimension> &,
                     const U0&),
                  Un(const typename GV::template Codim<0>::Entity &,
                     const FieldVector<typename GV::ctype, GV::dimension> &,
                     const Un&)...>
    makeReactionTerm(const ParameterTree &config, const SpeciesMap &speciesMap,
                     const GV &gv)
    {
      using std::begin;
      using std::end;

      using Element = typename GV::template Codim<0>::Entity;
      using LCoord = typename Element::Geometry::LocalCoordinate;
      using T0 = std::decay_t<decltype(std::declval<U0>()[0])>;

      auto type = config.get<std::string>("type");

      if(type == "linear")
      {
        auto target = speciesMap.index(config.get<std::string>("target"));
        auto factor = config.get("factor", T0(1));
        return [target,factor](const Element &, const LCoord &, const auto &u)
        {
          std::decay_t<decltype(u)> result;
          std::fill(begin(result), end(result), T0(0));
          result[target] = u[target]*factor;
          return result;
        };
      }

      if(type == "isomerase_conversion")
      {
        auto isomerase =
          speciesMap.index(config.get<std::string>("isomerase"));
        auto species1 = speciesMap.index(config.get<std::string>("species1"));
        auto species2 = speciesMap.index(config.get<std::string>("species2"));

        T0 f1, f2;
        if(config.hasKey("f1") || config.hasKey("f2"))
        {
          // configuration by f1 and f2
          if(config.hasKey("activity") ||
             config.hasKey("cf1") || config.hasKey("cf2"))
            DUNE_THROW(Exception, "isomerase_conversion: conflicting "
                       "configuration: give either conversion factors or "
                       "activity plus an equilibrium fraction, but not both");
          f1 = config.get<T0>("f1");
          f2 = config.get<T0>("f2");
        }
        else {
          // configuration by activity and one of cf1 or cf2
          T0 activity = config.get<T0>("activity");
          if(config.hasKey("cf1"))
          {
            if(config.hasKey("cf2"))
              DUNE_THROW(Exception, "isomerase_conversion: conflicting "
                         "configuration: both cf1 and cf2 given");
            f1 = activity * config.get<T0>("cf1");
            f2 = activity - f1;
          }
          else {
            if(!config.hasKey("cf2"))
              DUNE_THROW(Exception, "isomerase_conversion: conflicting "
                         "configuration: need one of cf1 and cf2");
            f2 = activity * config.get<T0>("cf2");
            f1 = activity - f2;
          }
        }

        return [=](const Element &, const LCoord &, const auto &u)
        {
          std::decay_t<decltype(u)> result;
          std::fill(begin(result), end(result), T0(0));
          auto rate = u[isomerase]*(u[species2]*f2 - u[species1]*f1);
          result[species1] =  rate;
          result[species2] = -rate;
          return result;
        };
      }

      DUNE_THROW(Exception, "Unknown reaction term type " << type);
    }

  } // namespace React
} // namespace Dune


#endif // DUNE_REACT_CONFIG_REACTION_HH
