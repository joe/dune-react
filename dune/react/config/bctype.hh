// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_REACT_CONFIG_BCTYPE_HH
#define DUNE_REACT_CONFIG_BCTYPE_HH

#include <algorithm>
#include <array>
#include <cstddef>
#include <map>
#include <numeric>
#include <string>
#include <utility>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>

#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>

#include <dune/react/config/match.hh>
#include <dune/react/config/species.hh>

namespace Dune {
  namespace React {

    struct BoundaryConditionMap : PDELab::ConvectionDiffusionBoundaryConditions
    {
      std::string name(Type type) const
      {
        switch(type) {
        case Dirichlet: return "dirichlet";
        case Neumann:   return "neumann";
        case Outflow:   return "outflow";
        case None:      return "none";
        default:
          DUNE_THROW(Exception, "Unknown "
                     "ConvectionDiffusionBoundaryConditions::Type with value "
                     << type);
        }
      }
      Type type(const std::string &name) const
      {
        if(name == "dirichlet") return Dirichlet;
        if(name == "neumann")   return Neumann;
        if(name == "outflow")   return Outflow;
        if(name == "none")      return None;

        DUNE_THROW(Exception, "Unknown "
                   "ConvectionDiffusionBoundaryConditions::Type with name " <<
                   name);
      }
    };

    template<class Param, class GV>
    class BoundaryConditionType
    {
      using Intersection = typename GV::Intersection;
      using LCoord = typename Intersection::Geometry::LocalCoordinate;

      struct MatchInfo {
        BoundaryMatchFunction<Param, GV> pred;
        std::vector<std::size_t> species;
        BoundaryConditionMap::Type type;
      };

      std::map<int, MatchInfo> matchers_;

    public:
      BoundaryConditionType(const ParameterTree &config,
                            const SpeciesMap &speciesMap, const GV &gv)
      {
        BoundaryConditionMap bcmap;

        for(const auto &key : config.getSubKeys())
        {
          const auto &sub = config.sub(key);

          MatchInfo info = {
            { sub.sub("match"), gv },
            {},
            bcmap.type(sub.get<std::string>("type"))
          };

          auto speciesNames =
            sub.get("species", std::vector<std::string>{"*"});

          if(speciesNames.empty())
            DUNE_THROW(Exception, key << ": The list of species that the "
                       "boundary condition applies to is empty; this is "
                       "meaningless");

          if(speciesNames.size() == 1 && speciesNames[0] == "*")
          {
            info.species.resize(speciesMap.size());
            std::iota(info.species.begin(), info.species.end(),
                      std::size_t(0));
          }
          else
          {
            info.species.reserve(speciesNames.size());
            for(const auto &name : speciesNames)
              info.species.push_back(speciesMap.index(name));
            std::sort(info.species.begin(), info.species.end());
            auto adjacent =
              std::adjacent_find(info.species.begin(), info.species.end());
            if(adjacent != info.species.end())
              DUNE_THROW(Exception, key << ": Species " <<
                         speciesMap.name(*adjacent) << " appears multiple "
                         "times");
          }

          auto priority = config.get("priority", info.pred.defaultPriority());

          matchers_.emplace(priority, std::move(info));
        }
      }

      std::array<BoundaryConditionMap::Type, Param::species()>
      operator()(const Param &params,
                 const Intersection &i, const LCoord &xl) const
      {
        std::array<BoundaryConditionMap::Type, Param::species()> bctypes;
        std::fill(bctypes.begin(), bctypes.end(),
                  BoundaryConditionMap::Dirichlet);
        for(const auto &matcher : matchers_)
          if(matcher.second.pred(params, i, xl))
            for(auto s : matcher.second.species)
              bctypes[s] = matcher.second.type;
        return bctypes;
      }
    };

  } // namespace React
} // namespace Dune


#endif // DUNE_REACT_CONFIG_BCTYPE_HH
