// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_REACT_CONFIG_MATCH_HH
#define DUNE_REACT_CONFIG_MATCH_HH

#include <algorithm>
#include <cstddef>
#include <functional>
#include <limits>
#include <string>
#include <utility>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>

#include <dune/react/utility/annotation.hh>

namespace Dune {
  namespace React {

    //! \brief Exception thrown when a `make...MatchFunctionFactor()` function
    //!        cannot handle a match type
    struct UnknownMatchFunctionType : Exception {};

    //! Match function signature for global match functions
    template<class Param, class GCoord>
    using GlobalMatchSignature = bool(const Param &, const GCoord &);

    //! Annotation tag used to store the default priority
    struct DefaultPriority { using type = int; };

    //! Make a match function factor that takes global coordinates
    /**
     * \tparam Param  Type of parameter class.
     * \tparam GCoord Type of global coordinates
     * \param  config `ParameterTree` to construct the factor from
     * \param  wrap   Decorates the factor, e.g. turns element coordinates
     *                into global coordinates.  The type of the return value
     *                should not depend on the type of `pred` --
     *                i.e. typically `wrap` should encapsulate the return
     *                value into a `std::function` or similar.  The default
     *                wrap just packs `pred` in a
     *                `std::function<GlobalMatchSignature<Param, GCoord> >`.
     * \return An annotated version of whatever `wrap(...)` returns.  There is
     *         one annotation: `DefaultPriority` of type `int`.
     */
    template<class Param, class GCoord, class Wrap>
    auto makeGlobalMatchFunctionFactor(const ParameterTree &config, Wrap wrap)
    {
      auto type = config.get("type", "default");
      bool invert = config.get("invert", false);
      auto postproc = [=](auto pred, int defprio = 0) {
        auto wrapped =
          wrap([=](auto... args) { return pred(args...) != invert; });
        return annotate(std::move(wrapped), DefaultPriority{}, defprio);
      };

      if(type == "default")
      {
        auto pred = [=](const Param &, const GCoord &) { return true; };
        return postproc(std::move(pred), std::numeric_limits<int>::min());
      }

      if(type == "bbox")
      {
        auto lower = config.get("lower", GCoord(0));
        auto upper = config.get("upper", GCoord(1));
        auto pred = [=](const Param &, const GCoord &xg)
          {
            bool inside = true;
            for(std::size_t d = 0; inside && d < xg.size(); ++d)
              inside &= lower[d] < xg[d] && xg[d] < upper[d];
            return inside;
          };
        return postproc(std::move(pred));
      }

      DUNE_THROW(UnknownMatchFunctionType,
                 "Unknown global match function type " << type);
    }

    template<class Param, class GCoord>
    Annotation<std::function<GlobalMatchSignature<Param, GCoord> >,
               DefaultPriority>
    makeGlobalMatchFunctionFactor(const ParameterTree &config)
    {
      auto wrap = [] (auto pred)
      {
        return std::function<GlobalMatchSignature<Param, GCoord> >(pred);
      };
      return makeGlobalMatchFunctionFactor<Param, GCoord>(config, wrap);
    }

    //! Match function signature for volume match functions
    template<class Param, class GV>
    using VolumeMatchSignature =
      bool(const Param &,
           const typename GV::template Codim<0>::Entity &,
           const typename GV::template Codim<0>::Geometry::LocalCoordinate &);

    //! Make a match function factor that takes global coordinates
    /**
     * \tparam Param  Type of parameter class.
     * \param  config `ParameterTree` to construct the factor from
     * \param  gv     `GridView` the match function is defined on.
     * \param  wrap   Decorates the factor, e.g. turns boundary coordinates
     *                into element coordinates.  The type of the return value
     *                should not depend on the type of `pred` --
     *                i.e. typically `wrap` should encapsulate the return
     *                value into a `std::function` or similar.  The default
     *                wrap just packs `pred` in a
     *                `std::function<VolumeMatchSignature<Param, GCoord> >`.
     * \return An annotated version of whatever `wrap(...)` returns.  There is
     *         one annotation: `DefaultPriority` of type `int`.
     */
    template<class Param, class GV, class Wrap>
    auto makeVolumeMatchFunctionFactor(const ParameterTree &config,
                                       const GV &gv, Wrap wrap)
    {
      using Element = typename GV::template Codim<0>::Entity;
      using LCoord = typename Element::Geometry::LocalCoordinate;
      using GCoord = typename Element::Geometry::GlobalCoordinate;

      // try global functions first
      try {
        auto gwrap = [=](auto pred)
        {
          // wrap expects a functor that can be called as pred(param, e, xl)
          auto vpred = [=](const Param &param,
                           const Element &e, const LCoord &xl) -> bool
          {
            return pred(param, e.geometry().global(xl));
          };
          return wrap(vpred);
        };
        return makeGlobalMatchFunctionFactor<Param, GCoord>(config, gwrap);
      }
      catch(UnknownMatchFunctionType) { /* nothing */ }

      // now try volume match functions -- none defined yet
      auto type = config.get<std::string>("type");
      // bool invert = config.get("invert", false);
      // auto postproc = [=](auto pred, int defprio = 0) {
      //   auto wrapped =
      //     wrap([=](auto... args) { return pred(args...) != invert; });
      //   return annotated(std::move(wrapped), DefaultPriority{}, defprio);
      // };

      DUNE_THROW(UnknownMatchFunctionType,
                 "Unknown volume match function type " << type);
    }

    template<class Param, class GV>
    Annotation<std::function<VolumeMatchSignature<Param, GV> >,
               DefaultPriority>
    makeVolumeMatchFunctionFactor(const ParameterTree &config, const GV &gv)
    {
      auto wrap = [] (auto pred)
      {
        return std::function<VolumeMatchSignature<Param, GV> >(pred);
      };
      return makeVolumeMatchFunctionFactor<Param>(config, gv, wrap);
    }

    //! Match function signature for boundary match functions
    template<class Param, class GV>
    using BoundaryMatchSignature =
      bool(const Param &,
           const typename GV::Intersection &,
           const typename GV::Intersection::Geometry::LocalCoordinate &);

    //! Make a match function factor that takes global coordinates
    /**
     * \tparam Param  Type of parameter class.
     * \param  config `ParameterTree` to construct the factor from
     * \param  gv     `GridView` the match function is defined on.
     * \param  wrap   Decorates the factor, e.g. turns boundary coordinates
     *                into element coordinates.  The type of the return value
     *                should not depend on the type of `pred` --
     *                i.e. typically `wrap` should encapsulate the return
     *                value into a `std::function` or similar.  The default
     *                wrap just packs `pred` in a
     *                `std::function<BoundaryMatchSignature<Param, GCoord> >`.
     * \return An annotated version of whatever `wrap(...)` returns.  There is
     *         one annotation: "default_priority" of type `int`.
     */
    template<class Param, class GV, class Wrap>
    auto makeBoundaryMatchFunctionFactor(const ParameterTree &config,
                                         const GV &gv, Wrap wrap)
    {
      using Intersection = typename GV::Intersection;
      using LCoord = typename Intersection::Geometry::LocalCoordinate;
      using GCoord = typename Intersection::Geometry::GlobalCoordinate;

      // try global functions first
      try {
        auto gwrap = [=](auto pred)
        {
          // wrap expects a functor that can be called as pred(param, i, xl)
          auto bpred = [=](const Param &param,
                           const Intersection &i, const LCoord &xl) -> bool
          {
            return pred(param, i.geometry().global(xl));
          };
          return wrap(bpred);
        };
        return makeGlobalMatchFunctionFactor<Param, GCoord>(config, gwrap);
      }
      catch(UnknownMatchFunctionType) { /* nothing */ }

      // try volume functions next
      try {
        auto ewrap = [=](auto pred)
        {
          // wrap expects a functor that can be called as pred(param, i, xl)
          auto bpred = [=](const Param &param,
                           const Intersection &i, const LCoord &xl) -> bool
          {
            return pred(param, i.inside(), i.geometryInInside().global(xl));
          };
          return wrap(bpred);
        };
        return makeVolumeMatchFunctionFactor<Param>(config, gv, ewrap);
      }
      catch(UnknownMatchFunctionType) { /* nothing */ }

      // now try boundary match functions
      auto type = config.get<std::string>("type");
      bool invert = config.get("invert", false);
      auto postproc = [=](auto pred, int defprio = 0) {
        auto wrapped =
          wrap([=](auto... args) { return pred(args...) != invert; });
        return annotate(std::move(wrapped), DefaultPriority{}, defprio);
      };

      if(type == "outflow")
      {
        auto pred = [=](const Param &param, const Intersection &i,
                        const LCoord &xl)
        {
          auto n = i.outerNormal(xl);
          auto b = param.b(i.inside(), i.geometryInInside().global(xl));
          return b * n > 0;
        };
        return postproc(std::move(pred));
      }

      DUNE_THROW(UnknownMatchFunctionType,
                 "Unknown boundary match function type " << type);
    }

    template<class Param, class GV>
    Annotation<std::function<BoundaryMatchSignature<Param, GV> >,
               DefaultPriority>
    makeBoundaryMatchFunctionFactor(const ParameterTree &config,
                                    const GV &gv)
    {
      auto wrap = [] (auto pred)
      {
        return std::function<BoundaryMatchSignature<Param, GV> >(pred);
      };
      return makeBoundaryMatchFunctionFactor<Param>(config, gv, wrap);
    };

    //! \brief Configurable predicate that can be evaluated with an
    //!        intersection and a local coordiante
    /**
     * This parses the `ParameterTree` given in `config` as a series of
     * factors that are combined together using logical and.  If all factors
     * match for a certain intersection and coordinate, then the whole
     * predicate matches and returns `true`, otherwise it returns `false`;
     * this result may be modified using an `invert` key.
     *
     * Factors are `ParameterTree`s of the form
     * \code
     * [factor_1]
     * type = default # always matches
     * # invert = true # never matches instead
     *
     * [factor_2]
     * type = bbox   # matches within a rectangular box, given in global coords
     * lower = -1 -1 # defaults to all components 0
     * upper = 1 1   # defaults to all components 1
     * # invert = true # matches outside of the box instead
     *
     * [factor_3]
     * type = outflow # matches on outflow boundaries (according to param.b())
     * # invert = true # matches on inflow boundaries instead
     * \endcode
     * The sense of each factor may be inverted using `invert = true`.
     *
     * The `ParameterTree` passed in `config` should either consist of a
     * single factor, possibly inverted, which is used as-is.  `type` is
     * optional for that single factor and defaults to `default`.
     * Alternatively, `config` may consist of several sub-trees with arbitrary
     * names, each describing a (possibly inverted) factor.  There may be a
     * key `invert = true` directly in `config` to invert the result of anding
     * all factors together (as opposed to inverting the individual factors).
     */
    template<class Param, class GV>
    class BoundaryMatchFunction
    {
      using Intersection = typename GV::Intersection;
      using LCoord = typename Intersection::Geometry::LocalCoordinate;
      using Signature = BoundaryMatchSignature<Param, GV>;
      std::vector<std::function<Signature> > factors_;
      bool invert_ = false;
      int defaultPriority_ = std::numeric_limits<int>::min();

    public:
      BoundaryMatchFunction(const ParameterTree &config, const GV &gv)
      {
        const auto &subKeys = config.getSubKeys();

        if(subKeys.size() > 0) {
          // configuration with factors
          if(config.hasKey("type"))
            DUNE_THROW(Exception, "BoundaryMatchFunction: both main type and "
                       "factors given");

          invert_ = config.get("invert", false);

          factors_.reserve(subKeys.size());
          for(const auto &key : subKeys) {
            auto result =
              makeBoundaryMatchFunctionFactor<Param>(config.sub(key), gv);
            defaultPriority_ = std::max(defaultPriority_,
                                        annotation(result, DefaultPriority{}));
            factors_.emplace_back(unannotated(std::move(result)));
          }
        }
        else {
          // configuration with a single main factor
          // also handles the default case (nothing configured)

          // don't handle invert here, that is picked up by the single
          // function
          auto result = makeBoundaryMatchFunctionFactor<Param>(config, gv);
          defaultPriority_ = std::max(defaultPriority_,
                                      annotation(result, DefaultPriority{}));
          factors_.emplace_back(unannotated(std::move(result)));
        }
      }

      int defaultPriority() const { return defaultPriority_; }

      bool operator()(const Param &param,
                      const Intersection &i, const LCoord &xl) const
      {
        bool match = true;
        for(const auto &pred : factors_)
          if(!pred(param, i, xl))
          {
            match = false;
            break;
          }
        return match != invert_;
      }
    };

  } // namespace React
} // namespace Dune


#endif // DUNE_REACT_CONFIG_MATCH_HH
