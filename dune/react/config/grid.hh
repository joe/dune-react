#ifndef DUNE_REACT_CONFIG_GRID_HH
#define DUNE_REACT_CONFIG_GRID_HH

#include <algorithm>
#include <cstddef>
#include <string>
#include <type_traits>
#include <utility>

#include <dune/common/classname.hh>
#include <dune/common/exceptions.hh>

#include <dune/react/utility/metatype.hh>

namespace Dune {
  namespace React {

    namespace GridNameOverloads {

      template<std::size_t priority, class SFINAE = void>
      struct ADLTag;
      template<> struct ADLTag<0> {};
      template<std::size_t priority>
      struct ADLTag<priority> : ADLTag<priority-1> {};

    } // namespace GridNameOverloads

    //! get a name for a grid manager
    /**
     * By default, determines the name of the grid manager from
     * `className<T>()`, by stripping of any template parameter lists and
     * namespaces.
     */
    template<class T>
    std::string gridname()
    {
      return gridname(GridNameOverloads::ADLTag<6>{}, MetaType<T>{});
    }

    namespace GridNameOverloads {

      template<class T>
      std::string gridname
      ( ADLTag<0, std::enable_if_t<std::is_same<std::decay_t<T>, T>::value> >,
        MetaType<T>)
      {
        auto name = className<T>();
        // remove template parameters, if any
        auto end = std::min(name.find('<'), name.size());
        // remove namespaces, if any
        auto begin = name.rfind(':', end);
        // if ':' was found, we need to advance one, so we don't include it
        // if ':' was not found, the result is npos, which is size_type(-1),
        // and advancing brings us to begin == 0, since size_type is an
        // unsigned type
        ++begin;
        return { std::move(name), begin, end-begin };
      }

      template<class T>
      std::string gridname
      ( ADLTag<0, std::enable_if_t<!std::is_same<std::decay_t<T>, T>::value> >,
        MetaType<T>)
      {
        return gridname<std::decay_t<T> >();
      }

    } // namespace GridNameOverloads

    //! thrown by makeGrid when the parameters do not fit the given grid type
    struct MakeGridBadType : Exception {};

  } // namespace React
} // namespace Dune

#endif // DUNE_REACT_CONFIG_GRID_HH
