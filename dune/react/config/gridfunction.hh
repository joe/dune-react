// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_REACT_CONFIG_GRIDFUNCTION_HH
#define DUNE_REACT_CONFIG_GRIDFUNCTION_HH

#include <algorithm>
#include <functional>
#include <iterator>
#include <string>
#include <utility>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/react/config/species.hh>

namespace Dune {
  namespace React {

    template<class U, class GV>
    std::function<U(const typename GV::template Codim<0>::Entity &,
                    const FieldVector<typename GV::ctype, GV::dimension> &)>
    makeGridFunctionTerm(const ParameterTree &config,
                         const SpeciesMap &speciesMap, const GV &gv)
    {
      using std::begin;
      using std::end;

      using Element = typename GV::template Codim<0>::Entity;
      using LCoord = typename Element::Geometry::LocalCoordinate;
      using T = decltype(+std::declval<U>()[0]);

      auto type = config.get<std::string>("type");

      if(type == "constant")
      {
        auto target = speciesMap.index(config.get<std::string>("target"));
        auto value = config.get("value", T(1));
        return [=](const Element &, const LCoord &)
        {
          U result;
          std::fill(begin(result), end(result), T(0));
          result[target] = value;
          return result;
        };
      }

      DUNE_THROW(Exception, "Unknown gridfunction term type " << type);
    }

  } // namespace React
} // namespace Dune


#endif // DUNE_REACT_CONFIG_GRIDFUNCTION_HH
