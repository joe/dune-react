// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_REACT_CONFIG_PARAMETER_HH
#define DUNE_REACT_CONFIG_PARAMETER_HH

#include <algorithm>
#include <array>
#include <cstddef>
#include <iostream>
#include <iterator>
#include <functional>
#include <vector>

#include <dune/common/parametertree.hh>

#include <dune/pdelab/constraints/common/constraintsparameters.hh>

#include <dune/react/config/bctype.hh>
#include <dune/react/config/gridfunction.hh>
#include <dune/react/config/reaction.hh>
#include <dune/react/config/species.hh>
#include <dune/react/parameter.hh>
#include <dune/react/utility/adept.hh>
#include <dune/react/utility/multifunction.hh>

namespace Dune {
  namespace React {

    template<typename GV, typename RF, std::size_t species>
    class ConfiguredModelProblem :
      public ReactionParameterInterface<GV, RF, species>
    {
      using BCType = PDELab::ConvectionDiffusionBoundaryConditions::Type;
      using Interface = ReactionParameterInterface<GV, RF, species>;

    public:
      using Traits = typename Interface::Traits;

    private:
      using Element = typename Traits::ElementType;
      using LCoord = typename Traits::DomainType;
      using Intersection = typename Traits::IntersectionType;
      using ICoord = typename Traits::IntersectionDomainType;
      using ADRF = AdeptADType<RF>;
      using U = std::array<RF, species>;
      using ADU = std::array<ADRF, species>;

      using ReactionTerm = U(const Element &, const LCoord &, const U &);
      using ADReactionTerm = ADU(const Element &, const LCoord &, const ADU &);
      std::vector<MultiFunction<ReactionTerm, ADReactionTerm> > reactionTerms_;

      BoundaryConditionType<ConfiguredModelProblem, GV> bctype_;

      using DirichletTerm = U(const Element &, const LCoord &);
      std::vector<std::function<DirichletTerm> > dirichletTerms_;

    public:
      ConfiguredModelProblem(const ParameterTree &config,
                             const SpeciesMap &speciesMap, const GV &gv) :
        bctype_(config.sub("bctype"), speciesMap, gv)
      {
        using std::begin;
        using std::end;

        // init reaction terms
        {
          const auto &sub = config.sub("reaction");
          const auto &keys = sub.getSubKeys();
          std::cerr << "Using the following reaction parameter blocks: ";
          std::copy(begin(keys), end(keys),
                    std::ostream_iterator<std::string>(std::cerr, " "));
          std::cerr << std::endl;
          reactionTerms_.reserve(keys.size());
          for(const auto &key : keys)
            reactionTerms_.emplace_back
              (makeReactionTerm<U, ADU>(sub.sub(key), speciesMap, gv));
        }

        // init dirichlet terms
        {
          const auto &sub = config.sub("dirichlet");
          const auto &keys = sub.getSubKeys();
          std::cerr << "Using the following Dirichlet parameter blocks: ";
          std::copy(begin(keys), end(keys),
                    std::ostream_iterator<std::string>(std::cerr, " "));
          std::cerr << std::endl;
          dirichletTerms_.reserve(keys.size());
          for(const auto &key : keys)
            dirichletTerms_.emplace_back
              (makeGridFunctionTerm<U>(sub.sub(key), speciesMap, gv));
        }
      }

      //! reaction/source/sink term
      template<class AnyU>
      AnyU f (const Element& e, const LCoord& x, const AnyU &u) const
      {
        AnyU F;
        std::fill(F.begin(), F.end(), 0.0);
        for(const auto &func : reactionTerms_)
          std::transform(F.begin(), F.end(), func(e, x, u).begin(), F.begin(),
                         std::plus<>());
        return F;
      }

      //! boundary condition type function
      auto bctype (const Intersection& i, const ICoord& xl) const
      {
        return bctype_(*this, i, xl);
      }

      //! Dirichlet boundary condition value
      /**
       * May be evaluated where some components are not on a Dirichlet
       * boundary, result may contain garbage values there.  But beware: is
       * also used to evaluate the initial conditions everywhere in the
       * domain.
       */
      U g (const Element& e, const LCoord& x) const
      {
        U G;
        std::fill(G.begin(), G.end(), 0.0);
        for(const auto &func : dirichletTerms_)
          std::transform(G.begin(), G.end(), func(e, x).begin(), G.begin(),
                         std::plus<>());
        return G;
      }

    };

  } // namespace React
} // namespace Dune


#endif // DUNE_REACT_CONFIG_PARAMETER_HH
