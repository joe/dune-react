#ifndef DUNE_REACT_CONFIG_STRUCTUREDGRID_HH
#define DUNE_REACT_CONFIG_STRUCTUREDGRID_HH

#include <exception>
#include <initializer_list>
#include <sstream>
#include <string>
#include <utility>

#include <dune/common/array.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/parametertree.hh>

#include <dune/grid/utility/structuredgridfactory.hh>

#include <dune/react/config/grid.hh>
#include <dune/react/utility/metatype.hh>

namespace Dune {
  namespace React {

    //! make a structured grid from a parameter tree
    /**
     * This uses the `StructuredGridFactory`.  The following parameters are
     * supported in `p`:
     * \code
     *   manager = grid manager name (default: empty = match any)
     *   dimension = dimension of grid (default: 0 = match any)
     *   lower = lower coordinate (default: all 0, a.k.a. origin)
     *   upper = upper coordinate (default: all 1)
     *   elements = number of divisions in each direction (default: all 1)
     *   elemtype = 'cube' or 'simplex' (default: empty = try both)
     *   refines = number of global refines to apply (default: 0)
     * \endcode
     *
     * The grid manager name is the one from `gridname()`.  Coordinates (as
     * well as `elements`) are a series of numbers, seperated by spaces.
     */
    template<class Grid>
    auto makeStructuredGrid(const ParameterTree &p)
    {
      auto manager = p.get("manager", "");
      if(manager != gridname<Grid>() && manager != "")
        DUNE_THROW(MakeGridBadType, "Expected 'manager = " << gridname<Grid>()
                   << "' or empty, got 'manager = " << manager << "'");

      auto dim = p.get("dimension", 0u);
      if(dim != 0 && dim != Grid::dimension)
        DUNE_THROW(MakeGridBadType, "Expected 'dimension = " << Grid::dimension
                   << "' or 0, got 'dimension = " << dim << "'");

      using Coord = FieldVector<typename Grid::ctype, Grid::dimensionworld>;
      Coord lower(0);
      lower = p.get("lower", lower);
      Coord upper(1);
      upper = p.get("upper", upper);

      auto elements = fill_array<unsigned, Grid::dimension>(1);
      elements = p.get("elements", elements);

      auto elemtype = p.get("elemtype", "");
      auto refines = p.get("refines", unsigned(0));

      struct BadType {};
      auto create = [&](const std::string &type) {
        using Factory = StructuredGridFactory<Grid>;
        if(type == "cube")
          return Factory::createCubeGrid(lower, upper, elements);
        if(type == "simplex")
          return Factory::createSimplexGrid(lower, upper, elements);
        throw BadType();
      };

      auto refined = [&](const std::string &type) {
        auto gridp = create(type);
        gridp->globalRefine(refines);
        return gridp;
      };

      if(elemtype == "") {
        std::ostringstream errors;

        for(auto type : { "cube", "simplex" })
        {
          try { return refined(type); }
          catch(const std::exception &e)
          {
            errors << "While trying " << type << ": " << e.what() << "\n";
          }
          catch(...)
          {
            errors << "While trying " << type << ": Unknown exception.\n";
          }
        }

        DUNE_THROW(MakeGridBadType,
                   "Neither createCubeGrid() nor createSimplexGrid() worked:\n"
                   << errors.str());
      }

      try { return refined(elemtype); } catch(BadType) { }

      DUNE_THROW(MakeGridBadType, "Unsupported 'elemtype = " << elemtype
                 << "', try either 'cube' or 'simplex', or leave empty to try "
                 << "both");
    }

    //! call f with a grid constructed from a parametertree
    /**
     * The grid will be constructed with makeStructuredGrid(), and handed over
     * to `f` as non-const lvalue reference.  `f` will be instantiated for all
     * grid types passed in `Grids`, but will only be invoked for one.
     */
    template<class... Grids, class F>
    void withStructuredGrid(const ParameterTree &p, F&& f)
    {
      bool called = false;
      std::ostringstream errors;
      errors << "Unable to construct a grid, tried the following grid "
             << "types:\n";
      Hybrid::forEach(MetaTuple<Grids...>(), [&](auto t) {
          if(!called) // skip further attempts after an attempt was successful
          {
            using Grid = typename decltype(t)::type;
            try {
              auto gridp = makeStructuredGrid<Grid>(p);
              called = true;
              std::forward<F>(f)(*gridp);
            }
            catch(const std::exception &e)
            {
              if(called) throw; // the exception came out of the call
              errors << className<Grid>() << ": " << e.what() << "\n";
            }
          }
        });
      if(!called) {
        errors << "Used the following parameters:\n";
        p.report(errors);
        DUNE_THROW(Exception, errors.str());
      }
    }

  } // namespace React
} // namespace Dune

#endif // DUNE_REACT_CONFIG_STRUCTUREDGRID_HH
