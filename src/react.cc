// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <type_traits>

#include <dune/grid/onedgrid.hh>
#include <dune/grid/yaspgrid.hh>

#include "react.hh"
// this declares external instantiations of instationary(), which is used by
// reactMain()
#include "react-extern.hh"

int main(int argc, char** argv)
{
  return reactMain<
    Dune::YaspGrid<1>,
    Dune::OneDGrid,
    Dune::YaspGrid<2>
    >(std::index_sequence<1, 2, 3>{},
      "react", argc, argv);
}
