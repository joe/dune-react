// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_REACT_SRC_REACT_EXTERN_HH
#define DUNE_REACT_SRC_REACT_EXTERN_HH

#include <type_traits>

#include <dune/common/parametertree.hh>

#include <dune/grid/onedgrid.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/react/config/species.hh>

#include "react.hh"

// OneDGrid
extern template
void instationary(const Dune::ParameterTree &,
                  const Dune::OneDGrid::LeafGridView &,
                  std::integral_constant<std::size_t, 1>,
                  const Dune::React::SpeciesMap &);
extern template
void instationary(const Dune::ParameterTree &,
                  const Dune::OneDGrid::LeafGridView &,
                  std::integral_constant<std::size_t, 2>,
                  const Dune::React::SpeciesMap &);
extern template
void instationary(const Dune::ParameterTree &,
                  const Dune::OneDGrid::LeafGridView &,
                  std::integral_constant<std::size_t, 3>,
                  const Dune::React::SpeciesMap &);

// YaspGrid<1>
extern template
void instationary(const Dune::ParameterTree &,
                  const Dune::YaspGrid<1>::LeafGridView &,
                  std::integral_constant<std::size_t, 1>,
                  const Dune::React::SpeciesMap &);
extern template
void instationary(const Dune::ParameterTree &,
                  const Dune::YaspGrid<1>::LeafGridView &,
                  std::integral_constant<std::size_t, 2>,
                  const Dune::React::SpeciesMap &);
extern template
void instationary(const Dune::ParameterTree &,
                  const Dune::YaspGrid<1>::LeafGridView &,
                  std::integral_constant<std::size_t, 3>,
                  const Dune::React::SpeciesMap &);

// YaspGrid<2>
extern template
void instationary(const Dune::ParameterTree &,
                  const Dune::YaspGrid<2>::LeafGridView &,
                  std::integral_constant<std::size_t, 1>,
                  const Dune::React::SpeciesMap &);
extern template
void instationary(const Dune::ParameterTree &,
                  const Dune::YaspGrid<2>::LeafGridView &,
                  std::integral_constant<std::size_t, 2>,
                  const Dune::React::SpeciesMap &);
extern template
void instationary(const Dune::ParameterTree &,
                  const Dune::YaspGrid<2>::LeafGridView &,
                  std::integral_constant<std::size_t, 3>,
                  const Dune::React::SpeciesMap &);

#endif // DUNE_REACT_SRC_REACT_EXTERN_HH
