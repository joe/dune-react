// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <type_traits>

#include <dune/grid/onedgrid.hh>

#include "react.hh"

int main(int argc, char** argv)
{
  // just a minimal example to quickly check that compilation works
  return reactMain<Dune::OneDGrid>
    (std::index_sequence<3>{},
     "react-minimal", argc, argv);
}
