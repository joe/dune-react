// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_REACT_SRC_REACT_HH
#define DUNE_REACT_SRC_REACT_HH

#include <array>
#include <cstddef>
#include <exception>
#include <iostream>
#include <memory>
#include <string>
#include <type_traits>
#include <utility>

#include <dune/common/exceptions.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/io/file/vtk/common.hh>
#include <dune/grid/io/file/vtk/vtksequencewriter.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/gridfunctionspace/powergridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/gridoperator/onestep.hh>
#include <dune/pdelab/instationary/implicitonestep.hh>
#include <dune/pdelab/instationary/onestepparameter.hh>
#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include <dune/pdelab/localoperator/l2.hh>
#include <dune/pdelab/newton/newton.hh>

#include <dune/react/config/parameter.hh>
#include <dune/react/config/species.hh>
#include <dune/react/config/structuredgrid.hh>
#include <dune/react/localoperator/reactfem.hh>
#include <dune/react/parameter.hh>

template<class GV, std::size_t species>
void instationary(const Dune::ParameterTree &config, const GV &gv,
                  std::integral_constant<std::size_t, species>,
                  const Dune::React::SpeciesMap &speciesMap)
{
  // constants and types
  // constexpr std::size_t dim = GV::dimension;
  // constexpr std::size_t dimw = GV::dimensionworld;
  using DF = typename GV::ctype;
  using RF = double;

  // temporal interval
  auto startTime = config.get("interval.start", 0.0);
  auto endOfTime = config.get("interval.end", 1.0);
  auto dt = config.get<double>("interval.step");

  // Parameters
  using Problem = Dune::React::ConfiguredModelProblem<GV, RF, species>;
  Problem problem(config.sub("params"), speciesMap, gv);
  problem.setTime(startTime);

  // Finite Element Map
  constexpr std::size_t degree = 1;
  using FEM = Dune::PDELab::QkLocalFiniteElementMap<GV, DF, RF, degree>;
  FEM fem(gv);

  // Constraints type
  using CON = Dune::PDELab::ConformingDirichletConstraints;

  // LA Backend
  using VB = Dune::PDELab::ISTL::VectorBackend<>;
  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  // TODO: 27 is too large / correct for all test cases, so should work fine
  MB mb(27);

  // Function space
  using CGFS = Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VB>;
  CGFS cgfs(gv,fem);
  using GFS = Dune::PDELab::PowerGridFunctionSpace<CGFS, species, VB>;
  GFS gfs(cgfs);
  // set names of subspaces from speciesMap
  for(std::size_t s = 0; s < species; ++s)
    gfs.child(s).name(speciesMap.name(s));

  // Determine where constraints apply
  using CC = typename GFS::template ConstraintsContainer<RF>::Type;
  CC cc;
  cc.clear();
  {
    using CBC = Dune::React::ReactionBoundaryConditionAdapter<Problem>;
    std::array<std::shared_ptr<CBC>, species> children;
    for(std::size_t s = 0; s < species; ++s)
      children[s] = std::make_shared<CBC>(problem, s);

    using BC = Dune::PDELab::PowerConstraintsParameters<CBC, species>;
    BC bctype(children);
    Dune::PDELab::constraints(bctype, gfs, cc);
  }

  // local operators
  using LOP =  Dune::React::ReactionFEM<Problem, FEM>;
  LOP lop(problem);
  typedef Dune::PDELab::PowerL2 MLOP;
  MLOP mlop(2*degree+2);

  // grid operator
  // spatial
  using GO0 =
    Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RF, RF, RF, CC, CC>;
  GO0 go0(gfs, cc, gfs, cc, lop, mb);
  // temporal
  using GO1 =
    Dune::PDELab::GridOperator<GFS, GFS, MLOP, MB, RF, RF, RF, CC, CC>;
  GO1 go1(gfs, cc, gfs, cc, mlop, mb);
  // combined (instationary)
  using IGO = Dune::PDELab::OneStepGridOperator<GO0,GO1>;
  IGO igo(go0,go1);

  // coefficient vector with initial and dirichlet bc values
  using U = typename IGO::Traits::Domain;
  U u(gfs, 0.0);
  // initialize DOFs from Dirichlet extension
  Dune::React::ReactionDirichletExtensionAdapter<Problem> g(gv,problem);
  Dune::PDELab::interpolate(g, gfs, u);

  // linear problem solver
  using LS = Dune::PDELab::ISTLBackend_OVLP_BCGS_ILU0<GFS, CC>;
  LS ls(gfs, cc);

  // nonlinear solver
  using PDESOLVER = Dune::PDELab::Newton<IGO, LS, U> ;
  PDESOLVER pdesolver(igo, ls);
  pdesolver.setReassembleThreshold(0.0);
  pdesolver.setVerbosityLevel(2);
  pdesolver.setReduction(1e-10);
  pdesolver.setMinLinearReduction(1e-4);
  pdesolver.setMaxIterations(25);
  pdesolver.setLineSearchMaxIterations(10);
  pdesolver.setParameters(config.sub("newton"));

  // time stepping scheme
  //Dune::PDELab::FractionalStepParameter<RF> method;
  Dune::PDELab::Alexander2Parameter<RF> method;
  Dune::PDELab::OneStepMethod<RF, IGO, PDESOLVER, U, U>
    osm(method,igo,pdesolver);
  osm.setVerbosityLevel(2);

  // output
  auto stationaryVTKWriter =
    std::make_shared<Dune::VTKWriter<GV> >(gv,Dune::VTK::conforming);
  Dune::VTKSequenceWriter<GV>
    vtkwriter(stationaryVTKWriter, config.get<std::string>("output.name"),
              config.get("output.path", ""),
              config.get("output.extendpath", ""));
  using VelocityGF =
    Dune::PDELab::ConvectionDiffusionVelocityExtensionAdapter<Problem>;
  VelocityGF velocityGF(gv, problem);
  if(config.get("output.add_velocity", false)) {
    vtkwriter.addVertexData
      (std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<VelocityGF> >
       (velocityGF, "velocity"));
  }
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,u);

  // time loop
  RF time = startTime;
  vtkwriter.write(time,Dune::VTK::appendedraw);

  while (time<endOfTime-1e-10)
  {
    problem.setTime(time+dt);

    // do time step
    U unew(gfs, 0.0);
    osm.apply(time, dt, u, g, unew);

    // accept time step
    u = std::move(unew);
    time += dt;

    // VTK output
    vtkwriter.write(time, Dune::VTK::appendedraw);
  }
}

// speciesCounts is expected to be an instance of std::index_sequence
template<class... Grids, class SpeciesCounts>
int reactMain(SpeciesCounts speciesCounts,
              const std::string &progname, int argc, char **argv)
{
  try {
    Dune::MPIHelper::instance(argc, argv);

    if(argc != 2)
    {
      std::cerr << "Usage: " << progname << " CONF\n"
                << "\n"
                << "Do a reaction simulation according to config file CONF."
                << std::endl;
      return 2;
    }

    Dune::ParameterTree config;
    Dune::ParameterTreeParser::readINITree(argv[1], config);

    Dune::React::SpeciesMap speciesMap(config.sub("species"));
    std::cout << "Using " << speciesMap.size() << " species: " << speciesMap
              << std::endl;

    Dune::React::withStructuredGrid<Grids...>
      (config.sub("grid"), [&](auto &grid) {
        Dune::Hybrid::switchCases
          (speciesCounts, speciesMap.size(),
           [&](auto species) {
             instationary(config, leafGridView(grid), species, speciesMap);
           },
           [=] {
             DUNE_THROW(Dune::Exception,
                        "Unexpected number of species " << speciesMap.size());
           });
      });

    return 0;
  }
  catch (std::exception &e){
    std::cerr << "Exception: " << e.what() << std::endl;
    return 2;
  }
}

#endif // DUNE_REACT_SRC_REACT_HH
