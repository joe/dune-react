// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cstddef>
#include <type_traits>

#include <dune/common/parametertree.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/react/config/species.hh>

#include "react.hh"

template
void instationary(const Dune::ParameterTree &,
                  const Dune::YaspGrid<1>::LeafGridView &,
                  std::integral_constant<std::size_t, 1>,
                  const Dune::React::SpeciesMap &);
