# .. cmake_module::
#
#    Find the Adept automatic differentiation library
#
#    You may set the following variables to modify the
#    behaviour of this module:
#
#    :ref:`ADEPT_ROOT`
#       Path list to search for Adept
#
#    Sets the following variables:
#
#    :code:`ADEPT_FOUND`
#       True if the Adept library was found.
#
# .. cmake_variable:: Adept_ROOT
#
#   You may set this variable to have :ref:`FindAdept` look for the Adept
#   library in the given path before inspecting system paths.
#


# search for location of header adept.h, only at positions given by the user
find_path(ADEPT_INCLUDE_DIR
  NAMES "adept.h"
  PATHS ${ADEPT_ROOT}
  PATH_SUFFIXES include
  NO_DEFAULT_PATH)
# try default paths now
find_path(ADEPT_INCLUDE_DIR
  NAMES "adept.h")

# look for library adept, only at positions given by the user
find_library(ADEPT_LIB adept
  PATHS ${ADEPT_ROOT}
  PATH_SUFFIXES lib lib64
  NO_DEFAULT_PATH
  DOC "Adept library")
# try default paths now
find_library(ADEPT_LIB adept)

macro(_AdeptCheck OPENMP)
  # do nothing if we found a previous working configuration
  if(NOT ADEPT_WORKS)
    include(CMakePushCheckState)
    include(CheckIncludeFileCXX)
    include(CheckCXXSymbolExists)

    # ADEPT_OPENMP_FLAG communicated the openmp flags to the invoker
    if(${OPENMP})
      set(ADEPT_OPENMP_FLAG -fopenmp)
    else(${OPENMP})
      set(ADEPT_OPENMP_FLAG "")
    endif(${OPENMP})

    # setup
    cmake_push_check_state()
    set(CMAKE_REQUIRED_INCLUDES
      ${CMAKE_REQUIRED_INCLUDES} ${ADEPT_INCLUDE_DIR})
    set(CMAKE_REQUIRED_FLAGS
      ${CMAKE_REQUIRED_FLAGS} ${ADEPT_OPENMP_FLAG})
    set(CMAKE_REQUIRED_LIBRARIES
      ${ADEPT_LIB} ${ADEPT_OPENMP_FLAG} ${CMAKE_REQUIRED_LIBRARIES})

    # check if header is accepted
    check_include_file_cxx("adept.h" ADEPT_HEADER_WORKS_OPENMP_${OPENMP})

    # check if library works
    if(ADEPT_HEADER_WORKS_OPENMP_${OPENMP})
      check_cxx_symbol_exists(adept::compiler_version "adept.h"
        ADEPT_LIB_WORKS_OPENMP_${OPENMP})
    endif(ADEPT_HEADER_WORKS_OPENMP_${OPENMP})

    #cleanup
    cmake_pop_check_state()

    set(ADEPT_WORKS ${ADEPT_LIB_WORKS_OPENMP_${OPENMP}})
  endif(NOT ADEPT_WORKS)
endmacro(_AdeptCheck)

_AdeptCheck(FALSE) # try without openmp
_AdeptCheck(TRUE) # try with openmp

# behave like a CMake module is supposed to behave
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  "ADEPT"
  DEFAULT_MSG
  ADEPT_WORKS
)
set_package_properties("Adept" PROPERTIES
  DESCRIPTION "A fast automatic differentiation library for C++"
  URL "http://www.met.rdg.ac.uk/clouds/adept/")

mark_as_advanced(
  ADEPT_LIB ADEPT_INCLUDE_DIR
  ADEPT_HEADER_WORKS_OPENMP_TRUE ADEPT_LIB_WORKS_OPENMP_TRUE
  ADEPT_HEADER_WORKS_OPENMP_FALSE ADEPT_LIB_WORKS_OPENMP_FALSE
  )

# if both headers and library are found, store results
if(ADEPT_FOUND)
  set(ADEPT_INCLUDE_DIRS ${ADEPT_INCLUDE_DIR})
  set(ADEPT_LIBRARIES ${ADEPT_LIB} ${ADEPT_OPENMP_FLAG})
  set(ADEPT_COMPILE_DEFINITIONS ENABLE_ADEPT=1)
  set(ADEPT_COMPILE_FLAGS ${ADEPT_OPENMP_FLAG})
  # log result
  file(APPEND ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeOutput.log
    "Determing location of Adept succeeded:\n"
    "Include directories: ${ADEPT_INCLUDE_DIRS}\n"
    "Libraries: ${ADEPT_LIBRARIES}\n"
    "Defines: ${ADEPT_COMPILE_DEFINITIONS}\n"
    "Compile Flags: ${ADEPT_COMPILE_FLAGS}\n\n")
else(ADEPT_FOUND)
  # log errornous result
  file(APPEND ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeError.log
    "Determing location of Adept failed:\n"
    "Include directories: ${Adept_INCLUDE_DIR}\n"
    "Libraries: ${ADEPT_LIB} (-fopenmp)\n"
    "Defines: ENABLE_ADEPT=1\n"
    "Compile Flags: (-fopenmp)\n\n")
endif(ADEPT_FOUND)

# set HAVE_ADEPT for config.h
set(HAVE_ADEPT ${ADEPT_FOUND})

# register all Adept related flags
if(HAVE_ADEPT)
  dune_register_package_flags(COMPILE_DEFINITIONS "${ADEPT_COMPILE_DEFINITIONS}"
                              COMPILE_OPTIONS "${ADEPT_COMPILE_FLAGS}"
                              LIBRARIES "${ADEPT_LIBRARIES}"
                              INCLUDE_DIRS "${ADEPT_INCLUDE_DIRS}")
endif(HAVE_ADEPT)
