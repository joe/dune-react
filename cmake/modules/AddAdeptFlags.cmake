# Defines the functions to use Adept
#
# .. cmake_function:: add_dune_adept_flags
#
#    .. cmake_param:: targets
#       :positional:
#       :single:
#       :required:
#
#       A list of targets to use Adept with.
#


function(add_dune_adept_flags _targets)
  if(ADEPT_FOUND)
    foreach(_target ${_targets})
      target_link_libraries(${_target} ${ADEPT_LIBRARIES})
      set_property(TARGET ${_target}
        APPEND_STRING
        PROPERTY COMPILE_FLAGS "${ADEPT_COMPILE_FLAGS} ")
      set_property(TARGET ${_target}
        APPEND_STRING
        PROPERTY COMPILE_DEFINITIONS "${ADEPT_COMPILE_DEFINITIONS} ")
      foreach(_path ${ADEPT_INCLUDE_DIRS})
        set_property(TARGET ${_target}
          APPEND_STRING
          PROPERTY COMPILE_FLAGS "-I${_path}")
      endforeach(_path ${ADEPT_INCLUDE_DIRS})
    endforeach(_target ${_targets})
  endif(ADEPT_FOUND)
endfunction(add_dune_adept_flags)
