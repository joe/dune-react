[species]
names = glucose fructose xylose_isomerase

[interval]
step = .0625
end = 10

[grid]
refines = 4

[output]
name = xylose_isomerase

# all reaction terms are summed up
[params.reaction.glucose_uptake]
# d u_target / dt += factor * u_target
type = linear
target = glucose
factor = -1

[params.reaction.xi_decay]
# d u_target / dt += factor * u_target
type = linear
target = xylose_isomerase
factor = -2

[params.reaction.conversion]
# There are two species 1, and 2 and a facilitating isomerase I with molar
# concentration c1(t), c2(t) and cI(t).  In all cases we assume that the
# conversion is lossless, i.e. for every molecule of species 1 disappearing
# because of the conversion a molecule of species 2 will appear and vice
# versa.
#
# Isolated Case
# =============
#
# Assume that there are no outside contributions of either species, and that
# the concentration of the isomerase does not change cI(t) = cI.  Then the
# total concentration of both species is constant
#
#   ct(t) = ct = c1(t) + c2(t) = const
#
# and the concentrations decay to some equilibrium concentrations c1(inf) and
# c2(inf) with a relaxation factor omega = cI * a, where a is the activity of
# the isomerase:
#
#   c1(t) = c1(inf) + (c1(0) - c1(inf)) * exp(-omega*t)
#   c2(t) = c2(inf) + (c2(0) - c2(inf)) * exp(-omega*t)
#
# It is convenient to specify the equilibrium concentrations as fractions of
# the total concentrations:
#
#   cf1(inf) = c1(inf) / ct
#   cf2(inf) = c1(inf) / ct
#
# The rate of change is then
#
#   d/dt c1 = omega * (c1(inf) - c1(t))
#           = omega * (cf1(inf)*c2(t) - cf2(inf)*c1(t))
#   d/dt c2 = omega * (c2(inf) - c2(t))
#           = omega * (cf2(inf)*c1(t) - cf1(inf)*c2(t))
#
# Note that d/dt c1 = - d/dt c2, which is just the losslessness assumption
# from the beginning.
#
# General Case
# ============
#
# In the general (non-isolated) case there may be outside contributions to the
# rate of change of the concentration, which means that the total
# concentration ct(t) and the concentration of the isomerase cI(t) are no
# longer assumed constant.  It is not possibly to specify the development of
# the concentrations without knowing those other contributions, but we can
# still specify the contributions to the rate of change:
# 
#   d/dt c1 += cI(t)*(c2(t)*f2 - c1(t)*f1)
#   d/dt c2 += cI(t)*(c1(t)*f1 - c2(t)*f2)
#
# Here f1 and f2 are conversion factors, which are still assumed constant.
# The correspondence to the restricted case is as follows:
#
#   a        = f1 + f2
#   cf1(inf) = f1 / a
#   cf2(inf) = f2 / a
#
# You can either specify the conversion factors f1 and f2, or the activity a
# and either one of the equilibrium fractions.
type = isomerase_conversion
isomerase = xylose_isomerase
species1 = glucose
species2 = fructose
#f1 = ...
#f2 = ...
activity = 5
cf1 = 0.42
#cf2 = ...

[params.dirichlet.isomerase]
type = constant
target = xylose_isomerase
value = 1

[params.dirichlet.glucose]
type = constant
target = glucose
value = 0

[params.dirichlet.fructose]
type = constant
target = fructose
value = 1

[params.bctype.default]
# when nothing else matches
type = neumann
# apply to all species (which is the default)
species = *
# matches everywhehre
match.type = default
# default has an implicit priority of int_min

[params.bctype.left]
type = dirichlet
# matches in a rectangular region
match.type = bbox
# highest matching priority wins
match.lower = -1e-6
match.upper = 1e-6
match.priority = 1

##[params.bctype.outflow]
#type = outflow
## matches where the velocity is outward
#match.type = outflow
## the default priority for everything but match=default is 0
