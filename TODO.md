A program that supports
- Instationary advection-diffusion-reaction for systems
- with a non-linear reaction term
- with the `jacobian_*()` methods computed by automatic differentiation
  + E.g. with [ADEPT](http://www.met.rdg.ac.uk/clouds/adept/)
- using conforming finite elements
- on 1D domains in nD, later trees in nD
  + But try not to limit to dim=1 too much
- with an arbitrary, but static number of components (species)
- Configurable via .cfg-files
  + For the reaction term there should be different models to select from

TODO steps:
- DONE: Make some test programs
- Make a configurable 2D application
  Configuration parameters should include:
  + spatial domain (grid)
  + temporal domain (start & end time)
  + number of components
  + Diffusion tensor fields
  + Velocity fields
  + reaction term fields
  + initial values
  + boundary type and boundary condition values (possibly at some point
    instationary)
  + linear solver
  + newton
  + time step method
  + time step adaptation parameters
  + output of solution etc
  + verbosity
  How should we parameterize fields?
  + We need constant fields
  + We need non-constant analytic fields (e.g. circular velocity, diffusion
    tensors given by Eberhard permeability)
  + At some point, we probably need fields that are read from files
  + We need tensor fields that are expanded from the diagonal, or even scalars
- Implement test programs in terms of the 2D application
- Implement AD
